package hsgildt.nl.gildtapp;

import android.support.test.espresso.Espresso;
import android.support.test.espresso.IdlingPolicies;
import android.support.test.espresso.IdlingRegistry;
import android.support.test.espresso.ViewAction;
import android.support.test.espresso.action.ViewActions;
import android.support.test.espresso.contrib.RecyclerViewActions;
import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;

import com.xamarin.testcloud.espresso.Factory;
import com.xamarin.testcloud.espresso.ReportHelper;

import org.junit.BeforeClass;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.nio.charset.Charset;
import java.util.Random;
import java.util.UUID;
import java.util.concurrent.TimeUnit;

import hsgildt.nl.gildtapp.view.LoginActivity;

import static android.support.test.InstrumentationRegistry.getInstrumentation;
import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.action.ViewActions.scrollTo;
import static android.support.test.espresso.action.ViewActions.typeText;
import static android.support.test.espresso.assertion.ViewAssertions.matches;
import static android.support.test.espresso.contrib.RecyclerViewActions.actionOnItemAtPosition;
import static android.support.test.espresso.matcher.ViewMatchers.hasDescendant;
import static android.support.test.espresso.matcher.ViewMatchers.hasErrorText;
import static android.support.test.espresso.matcher.ViewMatchers.isDisplayed;
import static android.support.test.espresso.matcher.ViewMatchers.withHint;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static android.support.test.espresso.matcher.ViewMatchers.withText;
import static com.schibsted.spain.barista.assertion.BaristaVisibilityAssertions.assertDisplayed;
import static com.schibsted.spain.barista.interaction.BaristaClickInteractions.clickOn;
import static com.schibsted.spain.barista.interaction.BaristaEditTextInteractions.writeTo;
import static com.schibsted.spain.barista.interaction.BaristaSleepInteractions.sleep;
import static junit.framework.TestCase.assertTrue;

@RunWith(AndroidJUnit4.class)
public class AuthTest {

    @Rule
    public ActivityTestRule<LoginActivity> loginActivityActivityTestRule = new ActivityTestRule<LoginActivity>(LoginActivity.class);

    public String username = "erik@gildt.nl";
    public String correct_password = "welcome01";
    public String wrong_password = "awrongpassword";

    @Rule
    public ReportHelper reportHelper = Factory.getReportHelper();

    @Test
    public void testSetup() {
        IdlingRegistry.getInstance().register(RetrofitBuilder.getInstance().idlingResource);
    }

    @Test
    public void login_success() {

        onView((withId(R.id.login_email)))
                .perform(typeText(username));

        onView(withId(R.id.login_password))
                .perform(typeText(correct_password));

        onView(withId(R.id.signin_btn))
                .perform(scrollTo())
                .check(matches(isDisplayed()));

        reportHelper.label("FilledInLoginScreen");

        onView(withId(R.id.signin_btn))
                .perform(click());

        onView(withId(R.id.events_recycler_view))
            .check(matches(isDisplayed()));

        reportHelper.label("UserIsLoggedInScreen");
    }

    @Test
    public void login_failure() {

        onView(withId(R.id.textView))
                .check(matches(withText(R.string.signin_text)));

        onView((withId(R.id.login_email)))
                .perform(typeText(username));

        onView(withId(R.id.login_password))
                .perform(typeText(wrong_password));

        onView(withId(R.id.signin_btn))
                .perform(scrollTo())
                .check(matches(isDisplayed()));

        reportHelper.label("FilledInLoginScreenWithWrongCredentials");

        onView(withId(R.id.signin_btn))
                .perform(click());

        onView(withId(R.id.signin_error_message))
                .check(matches(isDisplayed()));

        onView(withId(R.id.signin_error_message))
                .check(matches(withText("Gebruikersnaam en wachtwoord komen niet overheen")));

        reportHelper.label("ShowsErrorMessage");

    }

    @Test
    public void register_success() {
        String uniqueID = UUID.randomUUID().toString();

        clickOn("REGISTREER");
        assertDisplayed("Registreer");

        onView((withId(R.id.signup_username)))
                .perform(scrollTo())
                .perform(typeText("UI test"));


        onView((withId(R.id.signup_email)))
                .perform(scrollTo())
                .perform(typeText(uniqueID + "@ui-test.com"));


        onView((withId(R.id.signup_password)))
                .perform(scrollTo())
                .perform(typeText("test123"));


        onView((withId(R.id.signup_password_confirmation)))
                .perform(scrollTo())
                .perform(typeText("test123"));

        reportHelper.label("FilledInRegisterScreen");

        clickOn("REGISTREER");

        assertDisplayed("Evenementen");
        reportHelper.label("UserIsLoggedInScreen");

    }

    @Test
    public void register_failure() {

        clickOn("REGISTREER");
        assertDisplayed("Registreer");

        onView((withId(R.id.signup_username)))
                .perform(scrollTo())
                .perform(typeText("UI test"));


        onView((withId(R.id.signup_email)))
                .perform(scrollTo())
                .perform(typeText(username));


        onView((withId(R.id.signup_password)))
                .perform(scrollTo())
                .perform(typeText("test123"));


        onView((withId(R.id.signup_password_confirmation)))
                .perform(scrollTo())
                .perform(typeText("test123"));

        reportHelper.label("FilledInRegisterScreenWithExistingCredentials");

        clickOn("REGISTREER");

        //assertDisplayed("Email: has already been taken");

    }

}
