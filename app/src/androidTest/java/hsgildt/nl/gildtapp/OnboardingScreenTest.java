package hsgildt.nl.gildtapp;

import android.support.test.espresso.IdlingRegistry;
import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;

import com.xamarin.testcloud.espresso.Factory;
import com.xamarin.testcloud.espresso.ReportHelper;

import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import hsgildt.nl.gildtapp.view.OnboardingActivity;

import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.action.ViewActions.swipeLeft;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static com.schibsted.spain.barista.assertion.BaristaVisibilityAssertions.assertDisplayed;
import static com.schibsted.spain.barista.interaction.BaristaClickInteractions.clickOn;

@RunWith(AndroidJUnit4.class)
public class OnboardingScreenTest {

    @Rule
    public ActivityTestRule<OnboardingActivity> onboardingActivityActivityTestRule = new ActivityTestRule<OnboardingActivity>(OnboardingActivity.class);

    @Rule
    public ReportHelper reportHelper = Factory.getReportHelper();

    @Test
    public void scrollThroughOnboarding() {
        assertDisplayed("Welkom!");
        reportHelper.label("ShowsWelkomOnboarding");
        onView(withId(R.id.onboarding_view_pager))
                .perform(swipeLeft());
        assertDisplayed("Agenda");
        reportHelper.label("ShowsAgendaOnboarding");
        onView(withId(R.id.onboarding_view_pager))
                .perform(swipeLeft());
        assertDisplayed("Deals");
        reportHelper.label("ShowsDealsOnboarding");
        onView(withId(R.id.onboarding_view_pager))
                .perform(swipeLeft());
        assertDisplayed("Stempelkaart");
        reportHelper.label("ShowsStempelkaartOnboarding");
        onView(withId(R.id.onboarding_view_pager))
                .perform(swipeLeft());
        assertDisplayed("Jukebox");
        reportHelper.label("ShowsJukeboxOnboarding");
        onView(withId(R.id.onboarding_view_pager))
                .perform(swipeLeft());
        assertDisplayed("Foto's");
        reportHelper.label("ShowsFotosOnboarding");

        clickOn("BEGIN");

        assertDisplayed("Registreer");
        reportHelper.label("ShowsRegisterScreen");
    }

    @Test
    public void skipOnboarding() {
        clickOn("SKIP");

        assertDisplayed("Registreer");
        reportHelper.label("ShowsRegisterScreen");
    }
}
