package hsgildt.nl.gildtapp;

import android.support.test.espresso.IdlingRegistry;
import android.support.test.espresso.NoMatchingViewException;
import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;

import com.xamarin.testcloud.espresso.Factory;
import com.xamarin.testcloud.espresso.ReportHelper;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import hsgildt.nl.gildtapp.view.EventActivity;
import hsgildt.nl.gildtapp.view.LoginActivity;

import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.action.ViewActions.scrollTo;
import static android.support.test.espresso.action.ViewActions.typeText;
import static android.support.test.espresso.assertion.ViewAssertions.matches;
import static android.support.test.espresso.contrib.RecyclerViewActions.actionOnItemAtPosition;
import static android.support.test.espresso.matcher.ViewMatchers.isDisplayed;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static com.schibsted.spain.barista.assertion.BaristaVisibilityAssertions.assertDisplayed;
import static com.schibsted.spain.barista.internal.viewaction.SleepViewAction.sleep;

@RunWith(AndroidJUnit4.class)
public class EventScreenTest {

    @Rule
    public ActivityTestRule<EventActivity> eventActivityActivityTestRule = new ActivityTestRule<EventActivity>(EventActivity.class);

    @Rule
    public ReportHelper reportHelper = Factory.getReportHelper();

    @Test
    public void testSetup() {
        IdlingRegistry.getInstance().register(RetrofitBuilder.getInstance().idlingResource);
    }
    @Test
    public void showFirstEventInList() {
        onView(withId(R.id.events_recycler_view))
                .check(matches(isDisplayed()));
        reportHelper.label("ShowsListWithEvents");
        onView(withId(R.id.events_recycler_view))
              .perform(actionOnItemAtPosition(0, click()));
        assertDisplayed("Evenementen");
        reportHelper.label("ShowsDetailOfFirstEvent");
    }
}
