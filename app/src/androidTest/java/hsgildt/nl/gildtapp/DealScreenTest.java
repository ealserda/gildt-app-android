package hsgildt.nl.gildtapp;

import android.support.test.espresso.IdlingRegistry;
import android.support.test.espresso.NoMatchingViewException;
import android.support.test.espresso.contrib.RecyclerViewActions;
import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;

import com.xamarin.testcloud.espresso.Factory;
import com.xamarin.testcloud.espresso.ReportHelper;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import hsgildt.nl.gildtapp.view.DealActivity;
import hsgildt.nl.gildtapp.view.EventActivity;

import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.action.ViewActions.swipeLeft;
import static android.support.test.espresso.action.ViewActions.swipeUp;
import static android.support.test.espresso.assertion.ViewAssertions.matches;
import static android.support.test.espresso.contrib.RecyclerViewActions.actionOnItemAtPosition;
import static android.support.test.espresso.matcher.ViewMatchers.isDisplayed;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static com.schibsted.spain.barista.assertion.BaristaVisibilityAssertions.assertDisplayed;
import static com.schibsted.spain.barista.interaction.BaristaListInteractions.scrollListToPosition;
import static com.schibsted.spain.barista.interaction.BaristaSleepInteractions.sleep;

@RunWith(AndroidJUnit4.class)
public class DealScreenTest {

    @Rule
    public ActivityTestRule<DealActivity> dealActivityActivityTestRule = new ActivityTestRule<DealActivity>(DealActivity.class);

    @Rule
    public ReportHelper reportHelper = Factory.getReportHelper();

    @Test
    public void testSetup() {
        IdlingRegistry.getInstance().register(RetrofitBuilder.getInstance().idlingResource);
    }

    @Test
    public void redeemFirstDeal() {
        onView(withId(R.id.deal_picker))
                .check(matches(isDisplayed()));

        onView(withId(R.id.deal_picker))
                .perform(swipeLeft());

        onView(withId(R.id.deal_picker))
                .perform(RecyclerViewActions.scrollToPosition(1));

        sleep(1500);
        reportHelper.label("ShowsSecondDeal");

        onView(withId(R.id.deal_picker))
                .perform(actionOnItemAtPosition(1, swipeUp()));
        assertDisplayed("Deal is ingediend!");
        reportHelper.label("ShowsDealIsRedeemedMessage");
    }
}
