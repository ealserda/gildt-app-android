package hsgildt.nl.gildtapp;

import android.app.Activity;
import android.app.Application;
import android.content.SharedPreferences;
import android.util.Log;

import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.InstanceIdResult;
import com.google.gson.Gson;

import java.util.Observable;
import java.util.concurrent.Executor;

import hsgildt.nl.gildtapp.model.User;
import hsgildt.nl.gildtapp.services.AuthService;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


// Controlling the auth token, can be observed to get notified whenever the user signsin/out
public class AuthController extends Observable {
    private static AuthController instance;
    private AuthService authService;

    protected AuthController() {
        authService = RetrofitBuilder.getInstance().getRetrofitBuilder().create(AuthService.class);
    }

    public static AuthController getInstance() {
        if(instance == null) {
            instance = new AuthController();
        }
        return instance;
    }

    public boolean isAuthenticated (){
        return getAuthToken() != null;
    }

    public String getAuthToken() {
        User.Response user = getUser();
        if(user != null) {
            return user.jwt;
        } else {
            return null;
        }
    }

    public User.Response getUser() {
        Gson gson = new Gson();
        String json = GildtApplication.getSharedPreferences().getString("user", null);
        User.Response user = gson.fromJson(json, User.Response.class);
        return user;
    }

    public void login(User.Response user) {
        FirebaseInstanceId.getInstance().getInstanceId().addOnSuccessListener(new OnSuccessListener<InstanceIdResult>() {
            @Override
            public void onSuccess(InstanceIdResult instanceIdResult) {
                String newToken = instanceIdResult.getToken();
                updateFcmToken(newToken);
            }
        });
        setUser(user);
    }

    public void updateFcmToken(final String token) {

        authService.updateFcmToken(new User.Fcm(token)).enqueue(new Callback<Response<Void>>() {
            @Override
            public void onResponse(Call<Response<Void>> call, Response<Response<Void>> response) {
                if(token == null) {
                    setUser(null);
                }
            }

            @Override
            public void onFailure(Call<Response<Void>> call, Throwable t) {
                if(token == null) {
                    setUser(null);
                }
            }
        });
    }

    // Set user, save in shared preferences for user settings
    public void setUser(User.Response user) {
        SharedPreferences.Editor editor = GildtApplication.getSharedPreferences().edit();
        if (user == null) {
            editor.remove("user");
        } else {
            Gson gson = new Gson();
            String json = gson.toJson(user);
            editor.putString("user", json);
        }
        editor.commit();
        setChanged();
        notifyObservers();
    }

    // Delete fcm token on logout, to unsubscribe from notifications
    synchronized public void logout () {
        updateFcmToken("");
    }
}
