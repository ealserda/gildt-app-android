package hsgildt.nl.gildtapp;

import android.app.Application;
import android.content.Context;
import android.content.SharedPreferences;

import com.microsoft.appcenter.AppCenter;
import com.microsoft.appcenter.analytics.Analytics;
import com.microsoft.appcenter.crashes.Crashes;
import com.securepreferences.SecurePreferences;

public class GildtApplication extends Application {

    private static Context context;
    private static SharedPreferences sharedPreferences;

    public void onCreate() {
        super.onCreate();
        // Appcenter config for analytics and crash reports
        AppCenter.start(this, "544ae739-8108-4ebf-9a0f-4e1145dd9b3f",
                Analytics.class, Crashes.class);

        GildtApplication.context = getApplicationContext();
        // Using SecurePreferences for obfuscating and encrypting data
        GildtApplication.sharedPreferences = new SecurePreferences(GildtApplication.getAppContext());
    }

    public static Context getAppContext() {
        return GildtApplication.context;
    }

    public static SharedPreferences getSharedPreferences() {
        return GildtApplication.sharedPreferences;
    }
}