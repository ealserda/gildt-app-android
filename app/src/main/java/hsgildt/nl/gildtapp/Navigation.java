package hsgildt.nl.gildtapp;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.Gallery;
import android.widget.TextView;

import org.w3c.dom.Text;

import hsgildt.nl.gildtapp.model.User;
import hsgildt.nl.gildtapp.services.AuthService;
import hsgildt.nl.gildtapp.view.DealActivity;
import hsgildt.nl.gildtapp.view.EventActivity;
import hsgildt.nl.gildtapp.view.GalleryActivity;
import hsgildt.nl.gildtapp.view.JukeboxActivity;
import hsgildt.nl.gildtapp.view.LoginActivity;
import hsgildt.nl.gildtapp.view.SettingsActivity;
import hsgildt.nl.gildtapp.view.StampsActivity;

// Handling navigation for all activities & sharing some common logic
public class Navigation extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener {
	public NavigationView navigationView;
	private TextView navHeaderTitleTextView;
	private View navHeaderView;
	private User.Response user;

	public void setupNavigation() {
		Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
		setSupportActionBar(toolbar);

		DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
		ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
				this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
		drawer.addDrawerListener(toggle);
		toggle.syncState();

		user = AuthController.getInstance().getUser();

		navigationView = findViewById(R.id.nav_view);
		navigationView.setNavigationItemSelectedListener(this);
		navHeaderView =  navigationView.getHeaderView(0);
		navHeaderTitleTextView = navHeaderView.findViewById(R.id.nav_header_title);
		if(user != null && user.username != null) {
			navHeaderTitleTextView.setText(getString(R.string.navigation_name, user.username));
		}
	}

	// Calculate pixes based on dp
	public int dpToPx(int dp) {
		float density = this.getResources()
				.getDisplayMetrics()
				.density;
		return Math.round((float) dp * density);
	}

	// Close navigation drawer on back press
	@Override
	public void onBackPressed() {
		DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
		if (drawer.isDrawerOpen(GravityCompat.START)) {
			drawer.closeDrawer(GravityCompat.START);
		} else {
			moveTaskToBack(true);
		}
	}

	// Handle all navigation items
	@SuppressWarnings("StatementWithEmptyBody")
	@Override
	public boolean onNavigationItemSelected(MenuItem item) {
		int id = item.getItemId();

		if (id == R.id.nav_events) {
			Intent n = new Intent(this, EventActivity.class);
			startActivity(n);
		} else if (id == R.id.nav_deals) {
			Intent n = new Intent(this, DealActivity.class);
			startActivity(n);
		} else if (id == R.id.nav_gallary) {
			Intent n = new Intent(this, GalleryActivity.class);
			startActivity(n);
		} else if (id == R.id.nav_jukebox) {
			Intent n = new Intent(this, JukeboxActivity.class);
			startActivity(n);
		} else if (id == R.id.nav_stamps) {
			Intent n = new Intent(this, StampsActivity.class);
			startActivity(n);
		} else if (id == R.id.nav_settings) {
			Intent n = new Intent(this, SettingsActivity.class);
			startActivity(n);
		} else if (id == R.id.nav_logout) {
			AuthController.getInstance().logout();
			Intent n = new Intent(this, LoginActivity.class);
			startActivity(n);
		}
		this.overridePendingTransition(0, 0);
		DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
		drawer.closeDrawers();
		return true;
	}

}
