package hsgildt.nl.gildtapp.view.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.List;
import java.util.Random;

import hsgildt.nl.gildtapp.R;
import hsgildt.nl.gildtapp.model.Stamp;

public class StampRecyclerViewAdapter extends RecyclerView.Adapter<StampRecyclerViewAdapter.ViewHolder> {

    private List<Stamp> stamp_card;
    private Context context;
    private int stampWidth;
    private Random rand;

    public StampRecyclerViewAdapter(Context context, List<Stamp> stamp_card, int stampWidth) {
        this.stamp_card = stamp_card;
        this.context = context;
        this.stampWidth = stampWidth;
        rand = new Random();
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int i) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.stamp_item, parent, false);
        view.getLayoutParams().width = stampWidth;
        view.getLayoutParams().height = stampWidth;
        ViewHolder holder = new ViewHolder(view);
        return holder;
    }

    // Randomize position / rotation of stamp
    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, int position) {
        Stamp stamp = stamp_card.get(position);
        if(stamp.verified_attendance) {
            viewHolder.stampImageView.setVisibility(View.VISIBLE);
            viewHolder.stampImageView.setRotation((float) rand.nextInt(150));
            viewHolder.stampImageView.setY(rand.nextInt(120) - 30);
            viewHolder.stampImageView.setX(rand.nextInt(120)- 60);
        } else {
            viewHolder.stampEventDate.setText(stamp.event_date_humanized);
        }
    }

    @Override
    public int getItemCount() {
        return stamp_card.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        ImageView stampImageView;
        TextView stampEventDate;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            stampImageView = itemView.findViewById(R.id.stamp_card_stamp);
            stampEventDate = itemView.findViewById(R.id.stamp_event_date);
        }
    }
}
