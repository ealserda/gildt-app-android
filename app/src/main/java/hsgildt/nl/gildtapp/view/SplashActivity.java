package hsgildt.nl.gildtapp.view;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;

import com.securepreferences.SecurePreferences;

import hsgildt.nl.gildtapp.AuthController;
import hsgildt.nl.gildtapp.GildtApplication;
import hsgildt.nl.gildtapp.R;

public class SplashActivity extends AppCompatActivity {
    private static int SPLASHSCREEN_TIMEOUT = 1000;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setTitle("");
        setContentView(R.layout.activity_splash);
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                Intent intent;
                if(AuthController.getInstance().isAuthenticated()){
                    intent = new Intent(SplashActivity.this, EventActivity.class);
                } else {
                    boolean onboardingSkip = GildtApplication.getSharedPreferences().getBoolean("onboardingSkip", false);
                    if(onboardingSkip) {
                        intent = new Intent(SplashActivity.this, LoginActivity.class);
                    } else {
                        intent = new Intent(SplashActivity.this, OnboardingActivity.class);
                    }
                }
                startActivity(intent);
                finish();
            }
        }, SPLASHSCREEN_TIMEOUT);
    }
}
