package hsgildt.nl.gildtapp.view;

import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import java.util.List;

import hsgildt.nl.gildtapp.R;
import hsgildt.nl.gildtapp.RetrofitBuilder;
import hsgildt.nl.gildtapp.model.Image;
import hsgildt.nl.gildtapp.services.GalleryDetailService;
import hsgildt.nl.gildtapp.view.adapter.DetailGalleryRecyclerViewAdapter;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class GalleryDetailActivity extends AppCompatActivity implements Callback<List<Image>>, SwipeRefreshLayout.OnRefreshListener
{
    private RecyclerView recyclerView;
    private DetailGalleryRecyclerViewAdapter adapter;
    private GalleryDetailService galleryDetailService;
    private String tagID;
    private String tagName;
    private GridLayoutManager layoutManager;
    private int tagIDint;
    private SwipeRefreshLayout swipeRefreshLayout;
    private FloatingActionButton fabGallery;
    private String uploaded;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_gallery_detail);
        setupLayout();

        if(getIntent().hasExtra("Tag ID") && getIntent().hasExtra("Tag name"))
        {
            Intent intent = getIntent();
            tagID = intent.getStringExtra("Tag ID");
            tagName = intent.getStringExtra("Tag name");

            if(getIntent().hasExtra("Uploaded"))
            {
                uploaded = intent.getStringExtra("Uploaded");

                if(uploaded.contains("true"))
                {
                    Toast.makeText(GalleryDetailActivity.this, "Foto is geupload", Toast.LENGTH_LONG).show();
                }
                else if(uploaded.contains("false"))
                {
                    Toast.makeText(GalleryDetailActivity.this, "Er is iets misgegaan", Toast.LENGTH_LONG).show();
                }
            }

            this.galleryDetailService = RetrofitBuilder.getInstance().getRetrofitBuilder().create(GalleryDetailService.class);

            loadPhotos();
        }
    }

    public void setupLayout()
    {
        recyclerView = (RecyclerView) findViewById(R.id.gallery_detail_recyclerview2);
        layoutManager = new GridLayoutManager(this, 1);
        recyclerView.setLayoutManager(layoutManager);
        adapter = new DetailGalleryRecyclerViewAdapter(this);
        recyclerView.setAdapter(adapter);
        this.swipeRefreshLayout = findViewById(R.id.gallery_detail_swipe_refresh_layout);
        this.swipeRefreshLayout.setColorSchemeResources(R.color.colorPrimary,
                android.R.color.holo_green_dark,
                android.R.color.holo_orange_dark,
                android.R.color.holo_blue_dark);
        this.swipeRefreshLayout.setOnRefreshListener(this);
        fabGallery = findViewById(R.id.fab_gallery);
        fabGallery.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                gotoUpload();
            }
        });
        if(getIntent().hasExtra("Tag name"))
        {
            Intent intent = getIntent();
            this.setTitle(intent.getStringExtra("Tag name"));
        }
    }

    public void loadPhotos()
    {
        swipeRefreshLayout.setRefreshing(true);
        tagIDint = Integer.parseInt(tagID);
        galleryDetailService.imagesForTag(tagIDint).enqueue(this);
    }

    public void gotoUpload()
    {
        Intent uploadActivity = new Intent(this, UploadActivity.class);
        uploadActivity.putExtra("Tag ID", tagID);
        uploadActivity.putExtra("Tag name", tagName);
        startActivity(uploadActivity);
    }

    public void onResponse(Call<List<Image>> call, Response<List<Image>> response)
    {
        if (response.isSuccessful() && response.body() != null)
        {
            swipeRefreshLayout.setRefreshing(false);
            adapter.setData(response.body());
        }
        else
        {
            switch(response.code())
            {
                case 404:
                    Toast.makeText(GalleryDetailActivity.this, R.string.cant_find_server, Toast.LENGTH_LONG).show();
                    break;
                case 500:
                    Toast.makeText(GalleryDetailActivity.this, R.string.internal_error, Toast.LENGTH_LONG).show();
                    break;
                default:
                    Toast.makeText(GalleryDetailActivity.this, R.string.general_error, Toast.LENGTH_LONG).show();
            }
        }
    }

    @Override
    public boolean onSupportNavigateUp()
    {
        onBackPressed();
        return true;
    }

    @Override
    public void onFailure(Call<List<Image>> call, Throwable t)
    {
        swipeRefreshLayout.setRefreshing(false);
        Snackbar.make(swipeRefreshLayout, R.string.failure, Snackbar.LENGTH_INDEFINITE).setAction(
                R.string.retry, new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        loadPhotos();
                    }
                }).show();
    }

    @Override
    public void onRefresh()
    {
        loadPhotos();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item)
    {
        // Redirecting from upload back to tagged gallery
        switch (item.getItemId())
        {
            case android.R.id.home:
                if (uploaded != null)
                {
                    if (uploaded.contains("true") || uploaded.contains("false"))
                    {
                        Intent galleryActivity = new Intent(this, GalleryActivity.class);
                        startActivity(galleryActivity);
                        overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
                    }
                }
                else
                {
                    finish();
                }
        }
        return true;
    }
}



