package hsgildt.nl.gildtapp.view;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

import hsgildt.nl.gildtapp.AuthController;
import hsgildt.nl.gildtapp.R;
import hsgildt.nl.gildtapp.RetrofitBuilder;
import hsgildt.nl.gildtapp.model.User;
import hsgildt.nl.gildtapp.services.AuthService;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class RegisterActivity extends AppCompatActivity implements Callback<User.Response> {

    private EditText emailEditText;
    private EditText usernameEditText;
    private EditText passwordEditText;
    private EditText passwordConfirmationEditText;
    private TextView errorMessageLbl;
    private AuthService authService;
    private AuthController authController;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);

        this.emailEditText = findViewById(R.id.signup_email);
        this.usernameEditText = findViewById(R.id.signup_username);
        this.passwordEditText = findViewById(R.id.signup_password);
        this.passwordConfirmationEditText = findViewById(R.id.signup_password_confirmation);
        this.errorMessageLbl = findViewById(R.id.signup_error_message);

        authService = RetrofitBuilder.getInstance().getRetrofitBuilder().create(AuthService.class);
        this.authController = AuthController.getInstance();
    }

    public void register(View view) {
        String email = emailEditText.getText().toString();
        String username = usernameEditText.getText().toString();
        String password = passwordEditText.getText().toString();
        String passwordConfirmation = passwordConfirmationEditText.getText().toString();
        authService.register(new User(email, username, password, passwordConfirmation)).enqueue(this);;
    }

    public void navigateToLogin(View view) {
        Intent intent = new Intent(this, LoginActivity.class);
        startActivity(intent);
    }

    @Override
    public void onBackPressed() {
        moveTaskToBack(true);
    }

    @Override
    public void onResponse(Call<User.Response> call, Response<User.Response> response) {
        if (response.isSuccessful() && response.body() != null) {
            authController.login(response.body());
            Intent intent = new Intent(this, EventActivity.class);
            startActivity(intent);
        } else {
            JSONObject jsonObject = null;
            try {
                jsonObject = new JSONObject(response.errorBody().string());
                String message = jsonObject.getString("message");
                this.errorMessageLbl.setText(message);
            } catch (JSONException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public void onFailure(Call<User.Response> call, Throwable t) {

    }
}
