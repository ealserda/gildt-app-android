package hsgildt.nl.gildtapp.view.adapter;

import android.content.Context;
import android.graphics.Color;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import java.util.ArrayList;
import java.util.List;
import hsgildt.nl.gildtapp.R;
import hsgildt.nl.gildtapp.model.Jukebox;
import hsgildt.nl.gildtapp.view.JukeboxActivity;

public class JukeboxRecyclerViewAdapter extends RecyclerView.Adapter<JukeboxRecyclerViewAdapter.ViewHolder>
{
    private LayoutInflater inflater;
    private List<Jukebox> data = new ArrayList<Jukebox>();
    private boolean addedSong;
    private Context mContext;

    public JukeboxRecyclerViewAdapter(Context mContext)
    {
        this.mContext = mContext;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i)
    {
        View view = inflater.from(viewGroup.getContext()).inflate(R.layout.jukebox_item, viewGroup, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final JukeboxRecyclerViewAdapter.ViewHolder viewHolder, int i)
    {
        viewHolder.itemView.setBackgroundColor(Color.WHITE);

        TextView listCounterView = viewHolder.listCounter;
        TextView musicTitleView = viewHolder.musicTitle;
        TextView musicArtistView = viewHolder.musicArtist;
        TextView upvoteScoreView = viewHolder.upvoteScore;
        ImageView spotView = viewHolder.spot;
        ImageView upvoteView = viewHolder.upvote;
        ImageView downvoteView = viewHolder.downvote;

        spotView.setImageResource(R.mipmap.spot);
        upvoteView.setImageResource(R.mipmap.upvote);
        downvoteView.setImageResource(R.mipmap.upvote);

        upvoteScoreView.setTextColor(Color.parseColor("#464646"));
        listCounterView.setTextColor(Color.WHITE);
        musicArtistView.setTextColor(Color.parseColor("#464646"));
        musicTitleView.setTextColor(Color.parseColor("#464646"));

        if(i == 0 && addedSong)
        {
            viewHolder.itemView.setBackgroundColor(Color.parseColor("#23C03D"));
            upvoteScoreView.setTextColor(Color.WHITE);
            listCounterView.setTextColor(Color.parseColor("#23C03D"));
            musicTitleView.setTextColor(Color.WHITE);
            musicArtistView.setTextColor(Color.WHITE);
            spotView.setImageResource(R.mipmap.spot_white);
            upvoteView.setImageResource(R.mipmap.upvote_white);
            downvoteView.setImageResource(R.mipmap.upvote_white);

            addedSong = false;
        }

        listCounterView.setText(Integer.toString(i + 1));
        musicTitleView.setText(format(data.get(i).title));
        musicArtistView.setText(format(data.get(i).artist));
        upvoteScoreView.setText(Integer.toString(data.get(i).votes));

        if (data.get(i).did_vote == -1)
        {
            downvoteView.setColorFilter(Color.RED);
            upvoteView.clearColorFilter();
        }
        else if (data.get(i).did_vote == 1)
        {
            upvoteView.setColorFilter(Color.GREEN);
            downvoteView.clearColorFilter();
        }

        viewHolder.upvote.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                int pos = viewHolder.getAdapterPosition();

                if(mContext instanceof JukeboxActivity && data.get(pos).did_vote != 1)
                {
                    ((JukeboxActivity)mContext).upvoteSong(data.get(pos).id, pos);
                }
            }
        });

        viewHolder.downvote.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                int pos = viewHolder.getAdapterPosition();

                if(mContext instanceof JukeboxActivity && data.get(pos).did_vote != -1)
                {
                    ((JukeboxActivity)mContext).downvoteSong(data.get(pos).id, pos);
                }
            }
        });
    }

    @Override
    public int getItemCount()
    {
        return data.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder
    {
        TextView listCounter;
        TextView musicTitle;
        TextView musicArtist;
        TextView upvoteScore;
        ImageView spot;
        ImageView upvote;
        ImageView downvote;

        public ViewHolder(@NonNull View itemView)
        {
            super(itemView);

            listCounter = itemView.findViewById(R.id.list_counter);
            musicTitle = itemView.findViewById(R.id.music_title);
            musicArtist = itemView.findViewById(R.id.music_artist);
            upvoteScore = itemView.findViewById(R.id.upvote_score);
            spot = itemView.findViewById(R.id.spot);
            upvote = itemView.findViewById(R.id.upvote);
            downvote = itemView.findViewById(R.id.downvote);
        }
    }

    public void setData(List<Jukebox> data)
    {
        this.data = data;
        notifyDataSetChanged();
    }

    public void addedExtraSong()
    {
        addedSong = true;
    }


    public void changeEntry(Jukebox changedSong, int adapterPos)
    {
        data.set(adapterPos, changedSong);
        notifyDataSetChanged();
    }

    public String format(String text)
    {
        int length = text.length();

        if (length > 25)
        {
            String shortened = text.substring(0, 23);
            String dots = "..";
            String formatted = shortened + dots;

            return formatted;
        }
        else
        {
            return text;
        }
    }
}
