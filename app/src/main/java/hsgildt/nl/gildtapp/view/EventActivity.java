package hsgildt.nl.gildtapp.view;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import hsgildt.nl.gildtapp.Navigation;
import hsgildt.nl.gildtapp.R;
import hsgildt.nl.gildtapp.RetrofitBuilder;
import hsgildt.nl.gildtapp.model.Event;
import hsgildt.nl.gildtapp.services.EventService;
import hsgildt.nl.gildtapp.view.adapter.EventRecyclerViewAdapter;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import java.util.ArrayList;
import java.util.List;

public class EventActivity extends Navigation implements Callback<List<Event>>, SwipeRefreshLayout.OnRefreshListener {
	private List<Event> events;
	private EventRecyclerViewAdapter eventRecyclerViewAdapter;
	private RecyclerView recyclerView;
	private LinearLayoutManager layoutManager;
	private EventService eventService;
	private SwipeRefreshLayout swipeRefreshLayout;
	public static int ATTENDANCE_RESULT = 5;

	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_event);
		super.setupNavigation();

		setTitle(R.string.event_title);

		events = new ArrayList<>();
		eventService = RetrofitBuilder.getInstance().getRetrofitBuilder().create(EventService.class);

		setupLayout();
		loadEvents();
	}

	public void setupLayout() {
		eventRecyclerViewAdapter = new EventRecyclerViewAdapter(this, this.events);
		layoutManager = new LinearLayoutManager(this);
		recyclerView = findViewById(R.id.events_recycler_view);
		recyclerView.setLayoutManager(this.layoutManager);
		recyclerView.setAdapter(this.eventRecyclerViewAdapter);

		swipeRefreshLayout = findViewById(R.id.event_swipe_refresh_layout);
		swipeRefreshLayout.setColorSchemeResources(R.color.colorPrimary,
				android.R.color.holo_green_dark,
				android.R.color.holo_orange_dark,
				android.R.color.holo_blue_dark);

		swipeRefreshLayout.setOnRefreshListener(this);
	}

	@Override
	protected void onResume() {
		super.onResume();
		this.navigationView.setCheckedItem(R.id.nav_events);
	}

	// Load list with events
	private void loadEvents() {
		swipeRefreshLayout.setRefreshing(true);
		eventService.events().enqueue(this);
	}

	// Load event list callback
	@Override
	public void onResponse(Call<List<Event>> call, Response<List<Event>> response) {
		swipeRefreshLayout.setRefreshing(false);
		if (response.isSuccessful() && response.body() != null) {
			events.clear();
			events.addAll(response.body());
			eventRecyclerViewAdapter.notifyDataSetChanged();
		}
	}

	// Load event list callback failure
	@Override
	public void onFailure(Call<List<Event>> call, Throwable t) {
		swipeRefreshLayout.setRefreshing(false);
		Snackbar.make(swipeRefreshLayout, R.string.failure, Snackbar.LENGTH_INDEFINITE).setAction(
				R.string.retry, new View.OnClickListener() {
					@Override
					public void onClick(View v) {
						loadEvents();
					}
				}).show();
	}

	@Override
	public void onRefresh() {
		loadEvents();
	}

	// Find and update changed event with result from detail activity
	@Override
	protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		if(resultCode == RESULT_OK && requestCode == ATTENDANCE_RESULT) {
			Event newEvent = data.getParcelableExtra("event");
			for(Event event : events) {
				if(event.id == newEvent.id) {
					event.attendance_users = newEvent.attendance_users;
					event.attendance = newEvent.attendance;
					eventRecyclerViewAdapter.notifyDataSetChanged();
				}
			}
		}
	}
}
