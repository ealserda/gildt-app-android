package hsgildt.nl.gildtapp.view;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.Point;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Display;
import android.view.View;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import hsgildt.nl.gildtapp.Navigation;
import hsgildt.nl.gildtapp.R;
import hsgildt.nl.gildtapp.RetrofitBuilder;
import hsgildt.nl.gildtapp.SpacesItemDecoration;
import hsgildt.nl.gildtapp.model.Stamp;
import hsgildt.nl.gildtapp.services.StampService;
import hsgildt.nl.gildtapp.view.adapter.StampRecyclerViewAdapter;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.Manifest.permission.ACCESS_COARSE_LOCATION;
import static android.Manifest.permission.ACCESS_FINE_LOCATION;
import static android.Manifest.permission.CAMERA;

public class StampsActivity extends Navigation implements Callback<List<Stamp>>, LocationListener {

    private StampService stampService;
    private List<Stamp> stamp_card;
    private GridLayoutManager layoutManager;
    private Location location;
    private RecyclerView recyclerView;
    private LocationManager locationManager;
    private RelativeLayout claimStampButton;
    private ClaimButtonState claimButtonState;
    private TextView claimButtonText;
    private Location locationGildt;
    private RelativeLayout stampsRelativeLayout;
    // Location check is disabled for testing qr scanner
    private static boolean LOCATION_CHECK_DISABLED = true;

    final int MY_PERMISSIONS_REQUEST_CAMERA=0;
    final int MY_PERMISSIONS_REQUEST_LOCATION_FINE=1;
    final int MIN_DISTANCE_TO_GILDT = 50;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_stamps);
        super.setupNavigation();

        setTitle(R.string.stamp_title);

        locationGildt = new Location("Gildt");
        locationGildt.setLatitude(52.377604);
        locationGildt.setLongitude(4.6355626);

        stamp_card = new ArrayList<>();
        stampService = RetrofitBuilder.getInstance().getRetrofitBuilder().create(StampService.class);

        setupLayout();
        loadStampCard();
    }

    private void setupLayout() {
        navigationView.setCheckedItem(R.id.nav_stamps);
        locationManager = (LocationManager) getSystemService(this.LOCATION_SERVICE);
        claimStampButton = findViewById(R.id.stamp_card_button_layout);
        claimButtonState = ClaimButtonState.INITIAL_STATE;
        claimButtonText = findViewById(R.id.stamp_card_button_text);
        stampsRelativeLayout = findViewById(R.id.stamps_layout);

        Display display = getWindowManager().getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        int stampWidth = size.x / 3 - dpToPx(30);

        layoutManager = new GridLayoutManager(this, 3);
        recyclerView = findViewById(R.id.stamp_card_recycler_view);
        recyclerView.addItemDecoration(new SpacesItemDecoration(dpToPx(-10)));
        recyclerView.setLayoutManager(this.layoutManager);
        recyclerView.setAdapter(new StampRecyclerViewAdapter(this, this.stamp_card, stampWidth));
    }

    @Override
    protected void onStart() {
        super.onStart();
        setClaimButtonState(ClaimButtonState.INITIAL_STATE);
    }

    @Override
    protected void onPause() {
        super.onPause();
        locationManager.removeUpdates(this);
    }

    public void checkLocationClicked(View view) {
        checkLocation();
    }

    // Checks for available permissions
    // Permission not accepted? Request permission
    public void checkLocation() {
        if((ActivityCompat.checkSelfPermission(this, ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED)) {
            ActivityCompat.requestPermissions(this, new String[] {ACCESS_FINE_LOCATION}, MY_PERMISSIONS_REQUEST_LOCATION_FINE);
            return;
        }

        if((ActivityCompat.checkSelfPermission(this, CAMERA) != PackageManager.PERMISSION_GRANTED)) {
            ActivityCompat.requestPermissions(this, new String[] {CAMERA}, MY_PERMISSIONS_REQUEST_CAMERA);
            return;
        }

        setClaimButtonState(ClaimButtonState.WAITING_FOR_LOCATION);
        if(LOCATION_CHECK_DISABLED) {
            navigateToQrScanner();
        } else {
            locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0, 0, this);
        }
    }

    // Handles permissions for camera and location results
    // Declined? Ask again
    // Declined with don't ask again? Deep link to settings
    // Accepted? Check location
    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        if (requestCode == MY_PERMISSIONS_REQUEST_LOCATION_FINE || requestCode == MY_PERMISSIONS_REQUEST_CAMERA) {

            for (int i = 0, len = permissions.length; i < len; i++) {
                String permission = permissions[i];
                if (grantResults[i] == PackageManager.PERMISSION_DENIED) {
                    boolean askPermissionAllowed = false;
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                        askPermissionAllowed = shouldShowRequestPermissionRationale( permission );
                    }
                    if (!askPermissionAllowed) {
                        Snackbar.make(stampsRelativeLayout, R.string.stamp_location_permission, Snackbar.LENGTH_INDEFINITE).setAction(
                                R.string.stamp_change_permission, new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        Intent intent = new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
                                        Uri uri = Uri.fromParts("package", getPackageName(), null);
                                        intent.setData(uri);
                                        startActivity(intent);
                                    }
                                }).show();
                    } else if (Manifest.permission.ACCESS_FINE_LOCATION.equals(permission)) {
                        Snackbar.make(stampsRelativeLayout, R.string.stamp_location_permission_description, Snackbar.LENGTH_INDEFINITE).setAction(
                                R.string.stamp_check, new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        checkLocation();
                                    }
                                }).show();
                    } else if (Manifest.permission.CAMERA.equals(permission)) {
                        Snackbar.make(stampsRelativeLayout, R.string.stamp_camera_permission, Snackbar.LENGTH_INDEFINITE).setAction(
                                R.string.stamp_check, new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        checkLocation();
                                    }
                                }).show();
                    }
                } else if (grantResults[i] == PackageManager.PERMISSION_GRANTED) {
                    checkLocation();
                }
            }
        }
    }

    // Change claim stamp button dependent on state
    private void updateClaimStampButton() {
        if (claimButtonState == ClaimButtonState.WAITING_FOR_LOCATION) {
            claimStampButton.setOnClickListener(null);
            claimStampButton.setBackgroundColor(Color.GRAY);
            claimButtonText.setText(R.string.stamp_check_loaction);
        } else if (claimButtonState == ClaimButtonState.VALID_LOCATION) {
            claimButtonText.setText(R.string.stamp_scan_code);
            claimStampButton.setBackgroundColor(getResources().getColor(R.color.colorPrimary));
            claimStampButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    navigateToQrScanner();
                }
            });
        } else {
            claimStampButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    checkLocationClicked(v);
                }
            });
            claimButtonText.setText(R.string.stamp_claim_stamp);
            claimStampButton.setBackgroundColor(getResources().getColor(R.color.colorPrimary));
        }

        if (claimButtonState == ClaimButtonState.TOO_FAR_AWAY) {
            Toast.makeText(this, getString(R.string.stamp_too_far_error, MIN_DISTANCE_TO_GILDT), Toast.LENGTH_LONG).show();
        }
    }

    private void loadStampCard(){
        stampService.stamp_card().enqueue(this);
    }

    private void setClaimButtonState (ClaimButtonState state) {
        this.claimButtonState = state;
        this.updateClaimStampButton();
    }

    // Permissions granted? Distance < MIN_DISTANCE_TO_GILDT? -> Navigate to scanner
    private void navigateToQrScanner() {
        Intent i = new Intent(StampsActivity.this, QrScannerActivity.class);
        StampsActivity.this.startActivityForResult(i, 1);
    }

    @Override
    public void onResponse(Call<List<Stamp>> call, Response<List<Stamp>> response) {
        if (response.isSuccessful() && response.body() != null) {
            stamp_card.clear();
            stamp_card.addAll(response.body());
            recyclerView.getAdapter().notifyDataSetChanged();
        } else {

        }
    }

    @Override
    public void onFailure(Call<List<Stamp>> call, Throwable t) {
        Snackbar.make(stampsRelativeLayout, R.string.failure, Snackbar.LENGTH_INDEFINITE).setAction(
                R.string.retry, new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        loadStampCard();
                    }
                }).show();
    }

    // QR code response, try to claim stamp
    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(resultCode == RESULT_OK) {
            int qr_code = data.getIntExtra("qr_code", 0);

            if (qr_code != 0) {
                stampService.verify_attendance(qr_code).enqueue(new Callback<Stamp>() {
                    @Override
                    public void onResponse(Call<Stamp> call, Response<Stamp> response) {
                        if (response.isSuccessful() && response.body() != null) {
                            for (Stamp stamp : stamp_card) {
                                if (stamp.id == response.body().id) {
                                    stamp.verified_attendance = response.body().verified_attendance;
                                    recyclerView.getAdapter().notifyDataSetChanged();
                                    Toast.makeText(StampsActivity.this, R.string.stamp_added, Toast.LENGTH_LONG).show();
                                }
                            }
                        } else {
                            Toast.makeText(StampsActivity.this, R.string.stamp_qr_not_found, Toast.LENGTH_LONG).show();
                        }
                    }

                    @Override
                    public void onFailure(Call<Stamp> call, Throwable t) {
                        Toast.makeText(StampsActivity.this, R.string.failure, Toast.LENGTH_LONG).show();
                    }
                });
            }
        }
    }

    // Check location and distance to Gildt
    // Stop tracking location right away
    @Override
    public void onLocationChanged(Location location) {
        if (location != null) {
            float distance = locationGildt.distanceTo(location);
            locationManager.removeUpdates(this);
            if(distance < MIN_DISTANCE_TO_GILDT) {
                navigateToQrScanner();
                setClaimButtonState(ClaimButtonState.VALID_LOCATION);
            } else {
                setClaimButtonState(ClaimButtonState.TOO_FAR_AWAY);
            }
        }
    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {

    }

    @Override
    public void onProviderEnabled(String provider) {

    }

    @Override
    public void onProviderDisabled(String provider) {

    }

    // Different states of the claim stamp button
    public enum ClaimButtonState {
        WAITING_FOR_LOCATION,
        VALID_LOCATION,
        TOO_FAR_AWAY,
        INITIAL_STATE
    }
}
