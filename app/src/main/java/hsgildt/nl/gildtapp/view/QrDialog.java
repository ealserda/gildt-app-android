package hsgildt.nl.gildtapp.view;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v7.app.AppCompatDialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;

import hsgildt.nl.gildtapp.R;

public class QrDialog extends AppCompatDialogFragment {
    private EditText qrCodeEditText;
    private QrDialogListener listener;

    @Override
    public Dialog onCreateDialog(Bundle saveInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());

        LayoutInflater inflater = getActivity().getLayoutInflater();
        View view = inflater.inflate(R.layout.qr_dialog, null);

        builder.setView(view)
                .setTitle(R.string.qr_dialog_manual_code)
                .setNegativeButton(R.string.qr_dialog_cancel, new DialogInterface.OnClickListener(){
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                    }
                })
                .setPositiveButton(R.string.qr_dialog_send, new DialogInterface.OnClickListener(){

                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        String qrCode = qrCodeEditText.getText().toString();
                        listener.applyQrCode(qrCode);
                    }
                });
        qrCodeEditText = view.findViewById(R.id.qr_code_edit_text);

        return builder.create();
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        try {
            listener = (QrDialogListener) context;
        } catch(Exception e) {

        }
    }

    public interface QrDialogListener {
        void applyQrCode(String qrCode);
    }
}
