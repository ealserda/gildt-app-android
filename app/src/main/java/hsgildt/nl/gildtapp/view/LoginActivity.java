package hsgildt.nl.gildtapp.view;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import hsgildt.nl.gildtapp.AuthController;
import hsgildt.nl.gildtapp.R;
import hsgildt.nl.gildtapp.RetrofitBuilder;
import hsgildt.nl.gildtapp.model.User;
import hsgildt.nl.gildtapp.services.AuthService;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LoginActivity extends AppCompatActivity implements Callback<User.Response> {

    private EditText emailEditText;
    private EditText passwordEditText;
    private TextView errorMessageLbl;
    private AuthService authService;
    private AuthController authController;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        authService = RetrofitBuilder.getInstance().getRetrofitBuilder().create(AuthService.class);
        authController = AuthController.getInstance();

        setupLayout();
    }

    private void setupLayout() {
        this.emailEditText = findViewById(R.id.login_email);
        this.passwordEditText = findViewById(R.id.login_password);
        this.errorMessageLbl = findViewById(R.id.signin_error_message);
    }

    public void login(View view) {
        String email = emailEditText.getText().toString();
        String password = passwordEditText.getText().toString();
        authService.login(new User(email, password)).enqueue(LoginActivity.this);;
    }

    public void navigateToRegister(View view) {
        Intent intent = new Intent(this, RegisterActivity.class);
        startActivity(intent);
    }

    @Override
    public void onBackPressed() {
        moveTaskToBack(true);
    }

    @Override
    public void onResponse(Call<User.Response> call, Response<User.Response> response) {
        if (response.isSuccessful() && response.body() != null) {
            authController.login(response.body());
            Intent intent = new Intent(LoginActivity.this, EventActivity.class);
            startActivity(intent);
        } else {
            this.errorMessageLbl.setText(R.string.signin_error_text);
        }
    }

    @Override
    public void onFailure(Call<User.Response> call, Throwable t) {
        this.errorMessageLbl.setText(R.string.failure);
    }
}
