package hsgildt.nl.gildtapp.view;

import android.content.Intent;
import android.content.res.ColorStateList;
import android.graphics.Color;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.github.ksoichiro.android.observablescrollview.ObservableScrollView;
import com.github.ksoichiro.android.observablescrollview.ObservableScrollViewCallbacks;
import com.github.ksoichiro.android.observablescrollview.ScrollState;
import com.github.ksoichiro.android.observablescrollview.ScrollUtils;
import com.nineoldandroids.view.ViewHelper;

import org.w3c.dom.Text;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import hsgildt.nl.gildtapp.R;
import hsgildt.nl.gildtapp.RetrofitBuilder;
import hsgildt.nl.gildtapp.model.Event;
import hsgildt.nl.gildtapp.services.EventService;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions.withCrossFade;

public class EventDetailActivity extends AppCompatActivity  implements ObservableScrollViewCallbacks, Callback<Event> {
	public static final String CONTENT = "hsgildt.nl.gildtapp.EventDetailActivity.CONTENT";
	private View imageView;
	private View toolbarView;

	private FloatingActionButton attendanceFab;
	private TextView attendanceCount;
	private TextView attendanceList;

	private ObservableScrollView scrollView;
	private int parallaxImageHeight;
	private float scale;
	private Event event;
	private EventService eventService;

	private ImageView eventImage;
	private TextView eventTitle;
	private TextView eventDescription;
	private TextView eventDate;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_event_detail);
		Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
		setSupportActionBar(toolbar);

		getSupportActionBar().setDisplayHomeAsUpEnabled(true);
		getSupportActionBar().setDisplayShowHomeEnabled(true);

		scrollView = (ObservableScrollView) findViewById(R.id.scroll);
		imageView = findViewById(R.id.event_item_detail_image_view);
		toolbarView = findViewById(R.id.toolbar);
		toolbarView.setBackgroundColor(ScrollUtils.getColorWithAlpha(0, getResources().getColor(R.color.colorPrimary)));
		scrollView.setScrollViewCallbacks(this);
		event = getIntent().getParcelableExtra(CONTENT);
		eventService = RetrofitBuilder.getInstance().getRetrofitBuilder().create(EventService.class);

		parallaxImageHeight = 180;

		toolbar.setNavigationOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				finish();
			}
		});

		setupView();
	}

	private void setupView() {
		eventImage = findViewById(R.id.event_item_detail_image_view);
		eventTitle = findViewById(R.id.event_item_detail_title);
		eventDescription = findViewById(R.id.event_item_detail_description);
		attendanceList = findViewById(R.id.event_item_detail_attendance);
		attendanceCount = findViewById(R.id.event_item_detail_attendance_count);
		attendanceFab = findViewById(R.id.attendance_fab);
		eventDate = findViewById(R.id.event_item_detail_date);

		updateAttendance();

		Glide.with(this).load(event.image).into(eventImage);
		eventDescription.setText(event.description);
		eventTitle.setText(event.title);

		try {
			eventDate.setText(formatDate(event.event_date));
		} catch (ParseException e) {
			e.printStackTrace();
		}
	}

	// All UI interaction for listing attendance
	public void updateAttendance() {
		if(event.attendance) {
			attendanceFab.setColorFilter(getResources().getColor(R.color.colorPrimary));
			attendanceFab.setBackgroundTintList(ColorStateList.valueOf(Color.WHITE));
		} else {
			attendanceFab.setColorFilter(Color.WHITE);
			attendanceFab.setBackgroundTintList(ColorStateList.valueOf(getResources().getColor(R.color.colorPrimary)));
		}

		attendanceList.setText("");
		for (String name : event.attendance_users) {
			attendanceList.setText(attendanceList.getText() + "- " + name + "\n");
		}
		attendanceCount.setText(getString(R.string.event_signups, event.attendance_users.length));
	}

	// Convert timestamp to human readable format
	private String formatDate(String date) throws ParseException {
		DateFormat dateFormatter = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
		Date dateO = dateFormatter.parse(date);
		SimpleDateFormat formatter = new SimpleDateFormat("EEEE d MMMM yyyy HH:mm");
		return formatter.format(dateO);
	}


	public void toggleAttendance(View view) {
		Event.Attendance attendance = new Event.Attendance(!this.event.attendance);
		this.eventService.update_attendance(this.event.id, attendance).enqueue(this);
	}


	// Smooth parallex scroll effect for header image
	@Override
	public void onScrollChanged(int scrollY, boolean firstScroll, boolean dragging) {
		int baseColor = getResources().getColor(R.color.colorPrimary);
		float alpha = Math.min(1, (float) scrollY / parallaxImageHeight);
		toolbarView.setBackgroundColor(ScrollUtils.getColorWithAlpha(alpha, baseColor));
		ViewHelper.setTranslationY(imageView, scrollY / 2);
		scale = (float) (parallaxImageHeight - scrollY / 3) / parallaxImageHeight;
		if(scale < 0) {
			scale = 0;
		}
		ViewHelper.setScaleX(attendanceFab, scale);
		ViewHelper.setScaleY(attendanceFab, scale);
	}

	@Override
	protected void onRestoreInstanceState(Bundle savedInstanceState) {
		super.onRestoreInstanceState(savedInstanceState);
		onScrollChanged(scrollView.getCurrentScrollY(), false, false);
	}

	// Update attendance response, pass result to back event overview
	@Override
	public void onResponse(Call<Event> call, Response<Event> response) {
		if (response.isSuccessful() && response.body() != null) {
			this.event = response.body();
			this.updateAttendance();
			Intent resultIntent = new Intent();
			resultIntent.putExtra("event", event);
			setResult(RESULT_OK, resultIntent);
			if(event.attendance) {
				Toast.makeText(this, R.string.event_attendance_signup, Toast.LENGTH_LONG).show();
			} else{
				Toast.makeText(this, R.string.event_attendance_signoff, Toast.LENGTH_LONG).show();
			}
		} else {
			Toast.makeText(this, R.string.event_attendance_error, Toast.LENGTH_LONG).show();
		}
	}

	// Update attendance response failure
	@Override
	public void onFailure(Call<Event> call, Throwable t) {
		Toast.makeText(this, R.string.event_attendance_failure, Toast.LENGTH_LONG).show();
	}

	@Override
	public void onDownMotionEvent() { }

	@Override
	public void onUpOrCancelMotionEvent(ScrollState scrollState) { }
}
