package hsgildt.nl.gildtapp.view.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import com.bumptech.glide.Glide;
import java.util.ArrayList;
import java.util.List;
import hsgildt.nl.gildtapp.R;
import hsgildt.nl.gildtapp.model.Image;

public class DetailGalleryRecyclerViewAdapter extends RecyclerView.Adapter<DetailGalleryRecyclerViewAdapter.ViewHolder>
{
    private LayoutInflater inflater;
    private List<Image> data = new ArrayList<Image>();
    private Context context;

    public DetailGalleryRecyclerViewAdapter(Context context)
    {
        this.context = context;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i)
    {
        View view = inflater.from(viewGroup.getContext()).inflate(R.layout.gallery_detail_item, viewGroup, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final DetailGalleryRecyclerViewAdapter.ViewHolder viewHolder, int i)
    {
        ImageView galleryDetailPhotoView = viewHolder.galleryDetailPhoto;
        TextView photoDateView = viewHolder.photoDate;
        TextView photoDescriptionView = viewHolder.photoDescription;

        Glide.with(this.context).load("https://gildt.inholland-informatica.nl" + data.get(i).image.url).into(viewHolder.galleryDetailPhoto);
        photoDateView.setText(format(data.get(i).publish_date));
        photoDescriptionView.setText(data.get(i).description);
    }

    @Override
    public int getItemCount()
    {
        return data.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder
    {
        ImageView galleryDetailPhoto;
        TextView photoDate;
        TextView photoDescription;

        public ViewHolder(@NonNull View itemView)
        {
            super(itemView);
            galleryDetailPhoto = itemView.findViewById(R.id.gallery_detail_photo);
            photoDate = itemView.findViewById(R.id.photo_date);
            photoDescription = itemView.findViewById(R.id.photo_description);
        }
    }

    public void setData(List<Image> data)
    {
        this.data = data;
        notifyDataSetChanged();
    }

    public String format(String unformatted)
    {
        String year = unformatted.substring(0,4);
        String month = unformatted.substring(5,7);
        String day = unformatted.substring(8, 10);
        String hour = unformatted.substring(11,13);
        String minute = unformatted.substring(14, 16);
        String formatted = day + "-" + month + "-" + year + " " + hour + ":" + minute;
        return formatted;
    }
}
