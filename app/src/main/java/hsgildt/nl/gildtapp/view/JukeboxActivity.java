package hsgildt.nl.gildtapp.view;

import android.app.Activity;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.Toast;

import com.github.ksoichiro.android.observablescrollview.ObservableScrollView;
import com.github.ksoichiro.android.observablescrollview.ObservableScrollViewCallbacks;
import com.github.ksoichiro.android.observablescrollview.ScrollState;
import com.nineoldandroids.view.ViewHelper;

import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import hsgildt.nl.gildtapp.Navigation;
import hsgildt.nl.gildtapp.R;
import hsgildt.nl.gildtapp.RetrofitBuilder;
import hsgildt.nl.gildtapp.model.Jukebox;
import hsgildt.nl.gildtapp.services.JukeboxService;
import hsgildt.nl.gildtapp.view.adapter.JukeboxRecyclerViewAdapter;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

public class JukeboxActivity extends Navigation implements Callback<List<Jukebox>>, ObservableScrollViewCallbacks, SwipeRefreshLayout.OnRefreshListener
{
    private RecyclerView recyclerView;
    private JukeboxRecyclerViewAdapter adapter;
    private List<Jukebox> songs;
    private JukeboxService jukeboxService;
    private FloatingActionButton fabJukebox;
    private Jukebox addedSong;
    private boolean songHasBeenAdded;
    private int adapterPosition;
    private ObservableScrollView scrollView;
    private ImageView imageView;
    private SwipeRefreshLayout swipeRefreshLayout;
    private Timer timer;
    final Handler myHandler = new Handler();

    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_jukebox);
        super.setupNavigation();
        setupLayout();

        setTimerForAutoRefresh();
        checkForAddedSong();

        this.jukeboxService = RetrofitBuilder.getInstance().getRetrofitBuilder().create(JukeboxService.class);

        loadJukebox();
    }

    public void setupLayout()
    {
        this.navigationView.setCheckedItem(R.id.nav_jukebox);
        this.setTitle(R.string.jukebox);
        scrollView = (ObservableScrollView) findViewById(R.id.jukebox_scrollview);
        imageView = findViewById(R.id.jukebox_header_image);
        scrollView.setScrollViewCallbacks(this);
        this.swipeRefreshLayout = findViewById(R.id.jukebox_swipe_refresh_layout);
        this.swipeRefreshLayout.setColorSchemeResources(R.color.colorPrimary,
                android.R.color.holo_green_dark,
                android.R.color.holo_orange_dark,
                android.R.color.holo_blue_dark);
        this.swipeRefreshLayout.setOnRefreshListener(this);
        recyclerView = (RecyclerView) findViewById(R.id.jukebox_recyclerview);
        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setNestedScrollingEnabled(false);
        adapter = new JukeboxRecyclerViewAdapter(this);
        recyclerView.setAdapter(adapter);
        fabJukebox = findViewById(R.id.fab_jukebox);
        fabJukebox.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                addSong();
            }
        });
    }



    public void setTimerForAutoRefresh()
    {
        timer = new Timer();
        timer.schedule(new TimerTask()
        {
            @Override
            public void run()
            {
                autoRefresh();
            }
        }, 60000, 60000);
    }

    final Runnable myRunnable = new Runnable()
    {
        @Override
        public void run()
        {
            onRefresh();
        }
    };

    public void autoRefresh()
    {
        myHandler.post(myRunnable);
    }

    public void checkForAddedSong()
    {
        if(getIntent().hasExtra("Added song"))
        {
            Intent intent = getIntent();
            addedSong = intent.getExtras().getParcelable("Added song");
            songHasBeenAdded = true;
        }
    }

    @Override
    public void onResponse(Call<List<Jukebox>> call, Response<List<Jukebox>> response)
    {
        swipeRefreshLayout.setRefreshing(false);
        if (response.isSuccessful() && response.body() != null)
        {
            adapter.setData(response.body());

            if(songHasBeenAdded)
            {
                songs = response.body();
                songs.add(0, addedSong);
                adapter.addedExtraSong();
                adapter.setData(response.body());
                songHasBeenAdded = false;
            }
        }
        else
        {
            switch(response.code())
            {
                case 404:
                    Toast.makeText(JukeboxActivity.this, R.string.cant_find_server, Toast.LENGTH_LONG).show();
                    break;
                case 500:
                    Toast.makeText(JukeboxActivity.this, R.string.internal_error, Toast.LENGTH_LONG).show();
                    break;
                default:
                    Toast.makeText(JukeboxActivity.this, R.string.general_error, Toast.LENGTH_LONG).show();
            }
        }
    }

    @Override
    public void onFailure(Call<List<Jukebox>> call, Throwable t)
    {
        swipeRefreshLayout.setRefreshing(false);
        Snackbar.make(swipeRefreshLayout, R.string.failure, Snackbar.LENGTH_INDEFINITE).setAction(
                R.string.retry, new View.OnClickListener()
                {
                    @Override
                    public void onClick(View v)
                    {
                        loadJukebox();
                    }
                }).show();
    }

    public void addSong()
    {
        Intent jukeboxAdd = new Intent(this, JukeboxAddActivity.class);
        startActivity(jukeboxAdd);
    }

    public void upvoteSong(int songID, int adapterPos)
    {
        swipeRefreshLayout.setRefreshing(true);
        adapterPosition = adapterPos;
        jukeboxService.voteSong(songID).enqueue(new Callback<Jukebox>()
        {
            @Override
            public void onResponse(Call<Jukebox> call, Response<Jukebox> response)
            {
                swipeRefreshLayout.setRefreshing(false);
                if (response.isSuccessful() && response.body() != null)
                {
                    adapter.changeEntry(response.body(), adapterPosition);
                }
            }

            @Override
            public void onFailure(Call<Jukebox> call, Throwable t)
            {
                swipeRefreshLayout.setRefreshing(false);
                showFail();
            }
        });
    }

    public void downvoteSong(int songID, int adapterPos)
    {
        swipeRefreshLayout.setRefreshing(true);
        adapterPosition = adapterPos;
        jukeboxService.downvoteSong(songID).enqueue(new Callback<Jukebox>()
        {
            @Override
            public void onResponse(Call<Jukebox> call, Response<Jukebox> response)
            {
                swipeRefreshLayout.setRefreshing(false);
                if (response.isSuccessful() && response.body() != null)
                {
                    adapter.changeEntry(response.body(), adapterPosition);
                }
            }

            @Override
            public void onFailure(Call<Jukebox> call, Throwable t)
            {
                swipeRefreshLayout.setRefreshing(false);
                showFail();
            }
        });
    }



    public void showFail()
    {
        Toast.makeText(this, R.string.voting_error, Toast.LENGTH_LONG).show();
    }


    @Override
    public void onRefresh()
    {
        loadJukebox();
    }

    public void loadJukebox()
    {
        swipeRefreshLayout.setRefreshing(true);
        jukeboxService.songs().enqueue(this);
    }

    @Override
    public void onScrollChanged(int scrollY, boolean firstScroll, boolean dragging)
    {
        ViewHelper.setTranslationY(imageView, scrollY / 2);
    }

    @Override
    public void onDownMotionEvent() { }

    @Override
    public void onUpOrCancelMotionEvent(ScrollState scrollState) { }
}
