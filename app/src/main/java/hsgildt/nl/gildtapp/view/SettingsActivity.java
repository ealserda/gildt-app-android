package hsgildt.nl.gildtapp.view;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.CompoundButton;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;
import hsgildt.nl.gildtapp.AuthController;
import hsgildt.nl.gildtapp.Navigation;
import hsgildt.nl.gildtapp.R;
import hsgildt.nl.gildtapp.RetrofitBuilder;
import hsgildt.nl.gildtapp.model.Notifications;
import hsgildt.nl.gildtapp.model.User;
import hsgildt.nl.gildtapp.services.SettingsService;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SettingsActivity extends Navigation implements Callback<Notifications>
{
    private SettingsService settingsService;
    private SharedPreferences preferences;
    private SharedPreferences.Editor editor;
    private String notifications;
    private String notificationsSaved;
    private Switch thursdayReminder;
    private Switch specialEventsReminder;
    private TextView logoutView;
    private TextView settingsGreetingTextView;
    private boolean thursdayReminderValue = false;
    private boolean specialEventsReminderValue = false;
    private boolean initialSwitch;
    public static final String SETTINGS = "settings";
    private User.Response user;

    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings);
        super.setupNavigation();
        setupLayout();

        toggleSwitches(user.notification);
    }

    public void setupLayout()
    {
        this.navigationView.setCheckedItem(R.id.nav_settings);
        this.settingsGreetingTextView = findViewById(R.id.settings_greeting);
        this.setTitle("Instellingen");
        this.settingsService = RetrofitBuilder.getInstance().getRetrofitBuilder().create(SettingsService.class);
        this.user = AuthController.getInstance().getUser();
        this.settingsGreetingTextView.setText("Hey, " + user.username + "!");
        thursdayReminder = (Switch) findViewById(R.id.settings_thursday_reminder_switch);
        specialEventsReminder = (Switch) findViewById(R.id.settings_special_events_switch);
        logoutView = (TextView) findViewById(R.id.settings_logout);
        thursdayReminder.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener()
        {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked)
            {
                thursdayReminderValue = isChecked;
                if(!initialSwitch)
                {
                    saveState();
                }
            }
        });
        specialEventsReminder.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener()
        {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked)
            {
                specialEventsReminderValue = isChecked;
                if(!initialSwitch)
                {
                    saveState();
                }
            }
        });
        logoutView.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                logout();
            }
        });
    }

    public void logout()
    {
        AuthController.getInstance().logout();
        Intent n = new Intent(this, LoginActivity.class);
        startActivity(n);
    }

    public void toggleSwitches(String notifications)
    {
        switch(notifications)
        {
            case "never":
                specialEventsReminder.setChecked(false);
                thursdayReminder.setChecked(false);
                break;
            case "special_events":
                specialEventsReminder.setChecked(true);
                thursdayReminder.setChecked(false);
                break;
            case "open":
                specialEventsReminder.setChecked(false);
                thursdayReminder.setChecked(true);
                break;
            case "both":
                specialEventsReminder.setChecked(true);
                thursdayReminder.setChecked(true);
                break;
            default:
                Toast.makeText(this, "Default in switch", Toast.LENGTH_LONG).show();
                break;
        }
    }


    public void saveState()
    {
        notificationsSaved = "";

        if (specialEventsReminderValue && !thursdayReminderValue)
        {
            notificationsSaved = "special_events";
        }
        else if (specialEventsReminderValue && thursdayReminderValue)
        {
            notificationsSaved = "both";
        }
        else if (!specialEventsReminderValue && thursdayReminderValue)
        {
            notificationsSaved = "open";
        }
        else if (!specialEventsReminderValue && !thursdayReminderValue)
        {
            notificationsSaved = "never";
        }

        Notifications notifications = new Notifications(notificationsSaved);
        settingsService.changeNotificationSettings(notifications).enqueue(this);
    }

    @Override
    public void onResponse(Call<Notifications> call, Response<Notifications> response)
    {
        if(response.isSuccessful() && response.body() != null)
        {
            user.notification = response.body().notification;
            AuthController.getInstance().setUser(user);
        }
        else
        {
            switch(response.code())
            {
                case 404:
                    Toast.makeText(SettingsActivity.this, R.string.cant_find_server, Toast.LENGTH_LONG).show();
                    break;
                case 500:
                    Toast.makeText(SettingsActivity.this, R.string.internal_error, Toast.LENGTH_LONG).show();
                    break;
                default:
                    Toast.makeText(SettingsActivity.this, R.string.general_error, Toast.LENGTH_LONG).show();
            }
        }
    }

    @Override
    public void onFailure(Call<Notifications> call, Throwable t)
    {
        Toast.makeText(this, "onFailure response", Toast.LENGTH_LONG).show();
    }
}


