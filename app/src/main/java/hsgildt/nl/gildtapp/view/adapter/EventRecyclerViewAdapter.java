package hsgildt.nl.gildtapp.view.adapter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;

import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;
import hsgildt.nl.gildtapp.R;
import hsgildt.nl.gildtapp.model.Event;
import hsgildt.nl.gildtapp.view.EventActivity;
import hsgildt.nl.gildtapp.view.EventDetailActivity;

import static com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions.withCrossFade;

public class EventRecyclerViewAdapter extends RecyclerView.Adapter<EventRecyclerViewAdapter.ViewHolder> {

    private Context context;
    private List<Event> events;

    public EventRecyclerViewAdapter(Context context, List<Event> events) {
        this.events = events;
        this.context = context;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int i) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.event_item, parent, false);
        ViewHolder holder = new ViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, final int position) {
        Event event = events.get(position);
        viewHolder.eventTitle.setText(event.title);
        viewHolder.eventDescription.setText(event.short_description);
        viewHolder.stampImage.setVisibility(event.theme_party ? View.VISIBLE : View.GONE);
        if(event.attendance_users.length >= 0) {
            viewHolder.eventAttendance.setText(Integer.toString(event.attendance_users.length));
        }
        RequestOptions options = new RequestOptions().centerCrop().circleCropTransform();
        Glide.with(this.context).load(event.image).apply(options).transition(withCrossFade()).into(viewHolder.eventImage);
        viewHolder.cardView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(context, EventDetailActivity.class);
                i.putExtra(EventDetailActivity.CONTENT, events.get(position));
                ((Activity) context).startActivityForResult(i, EventActivity.ATTENDANCE_RESULT);
            }
        });
    }

    @Override
    public int getItemCount() {
        return events.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        ImageView eventImage;
        ImageView stampImage;
        TextView eventTitle;
        TextView eventDescription;
        TextView eventAttendance;
        CardView cardView;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            eventImage = itemView.findViewById(R.id.event_item_image);
            stampImage = itemView.findViewById(R.id.event_item_stamp);
            eventTitle = itemView.findViewById(R.id.event_item_title);
            eventDescription = itemView.findViewById(R.id.event_item_description);
            eventAttendance = itemView.findViewById(R.id.event_item_attendance);
            cardView = itemView.findViewById(R.id.event_item);
        }
    }
}
