package hsgildt.nl.gildtapp.view.adapter;

import android.content.Context;
import android.graphics.Color;
import android.graphics.Point;
import android.os.Build;
import android.os.Handler;
import android.os.VibrationEffect;
import android.os.Vibrator;
import android.support.annotation.NonNull;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import org.w3c.dom.Text;

import java.util.List;

import hsgildt.nl.gildtapp.GildtApplication;
import hsgildt.nl.gildtapp.R;
import hsgildt.nl.gildtapp.model.Deal;
import hsgildt.nl.gildtapp.view.DealActivity;

import static com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions.withCrossFade;

public class DealRecyclerViewAdapter extends  RecyclerView.Adapter<DealRecyclerViewAdapter.ViewHolder> {

    private DealActivity context;
    private List<Deal> deals;
    private int cardWidth;

    public DealRecyclerViewAdapter(DealActivity context, List<Deal> deals, int cardWidth) {
        this.deals = deals;
        this.context = context;
        this.cardWidth = cardWidth;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int i) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.deal_item, parent, false);
        view.getLayoutParams().width = cardWidth;
        view.setOnTouchListener(context);
        ViewHolder holder = new ViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, int position) {
        Deal deal = deals.get(position);
        Glide.with(this.context).load(deal.image).transition(withCrossFade()).into(viewHolder.dealImage);
        viewHolder.dealTitle.setText(deal.title);
        viewHolder.dealDescription.setText(deal.description);
        viewHolder.dealFooter.setText(deal.expire_date_humanized);
        if(deal.deals_left != null && deal.deals_left > 0 && deal.deal_type.equals("usage")) {
            viewHolder.dealsLeft.setText(GildtApplication.getAppContext().getString(R.string.deal_usage_left, deal.deals_left));
        } else if (deal.deal_type.equals("timely")){
            viewHolder.dealsLeft.setText(GildtApplication.getAppContext().getString(R.string.deal_no_limit));
        } else {
            viewHolder.dealsLeft.setText(GildtApplication.getAppContext().getString(R.string.deal_limit_reached));
        }
    }

    @Override
    public int getItemCount() {
        return deals.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        ImageView dealImage;
        TextView dealTitle;
        TextView dealDescription;
        public TextView dealStateOverlay;
        TextView dealFooter;
        public TextView dealsLeft;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            dealImage = itemView.findViewById(R.id.deal_card_image);
            dealTitle = itemView.findViewById(R.id.deal_card_title);
            dealDescription = itemView.findViewById(R.id.deal_card_description);
            dealStateOverlay = itemView.findViewById(R.id.deal_state_overlay);
            dealFooter = itemView.findViewById(R.id.deal_card_footer);
            dealsLeft = itemView.findViewById(R.id.deal_card_coupons_left);
        }

        public void changeAndShowState (String text, DealState state, int delay) {
            dealStateOverlay.setVisibility(View.VISIBLE);
            if(state == DealState.SUCCESS) {
                dealStateOverlay.setBackgroundColor(itemView.getResources().getColor(R.color.colorPrimary));
                dealStateOverlay.setAlpha(0.9f);
                dealStateOverlay.setTextColor(Color.WHITE);
            } else {
                dealStateOverlay.setTextColor(Color.RED);
            }
            dealStateOverlay.setText(text);

            delayedHideOverlay(delay);
        }

        private void delayedHideOverlay( int delay) {
            Handler handler = new Handler();
            handler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    dealStateOverlay.setVisibility(View.GONE);
                    dealStateOverlay.setBackgroundColor(Color.WHITE);
                    dealStateOverlay.setAlpha(0.9f);
                }
            }, delay);
        }
    }

    public enum DealState {
        SUCCESS,
        ERROR,
        NETWORK_FAILURE
    }

}
