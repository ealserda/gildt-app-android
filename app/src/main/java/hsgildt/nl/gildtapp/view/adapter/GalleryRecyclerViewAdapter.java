package hsgildt.nl.gildtapp.view.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import com.bumptech.glide.Glide;
import java.util.ArrayList;
import java.util.List;
import hsgildt.nl.gildtapp.R;
import hsgildt.nl.gildtapp.view.GalleryActivity;
import static com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions.withCrossFade;

public class GalleryRecyclerViewAdapter extends RecyclerView.Adapter<GalleryRecyclerViewAdapter.ViewHolder>
{
    private LayoutInflater inflater;
    private List<hsgildt.nl.gildtapp.model.Tag> data = new ArrayList<hsgildt.nl.gildtapp.model.Tag>();
    private Context context;

    public GalleryRecyclerViewAdapter(Context context)
    {
        this.context = context;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i)
    {
        View view = inflater.from(viewGroup.getContext()).inflate(R.layout.gallery_item, viewGroup, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final GalleryRecyclerViewAdapter.ViewHolder viewHolder, int i)
    {
        ImageView tagThumbnailView = viewHolder.tagThumbnail;
        TextView tagTitleView = viewHolder.tagTitle;
        TextView photoCounterView = viewHolder.photoCounter;
        if (data.get(i).preview_image == null)
        {

        }
        else
        {
            Glide.with(this.context).load("https://gildt.inholland-informatica.nl" + data.get(i).preview_image.url).transition(withCrossFade()).into(viewHolder.tagThumbnail);
        }

        tagTitleView.setText(data.get(i).title);
        photoCounterView.setText(Integer.toString(data.get(i).number_of_images));
        viewHolder.itemView.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                int id = data.get(viewHolder.getAdapterPosition()).id;
                String name = data.get(viewHolder.getAdapterPosition()).title;

                if (context instanceof GalleryActivity)
                {
                    ((GalleryActivity) context).gotoDetailView(id, name);
                }
            }
        });
    }

    @Override
    public int getItemCount()
    {
        return data.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder
    {
        ImageView tagThumbnail;
        TextView tagTitle;
        TextView photoCounter;

        public ViewHolder(@NonNull View itemView)
        {
            super(itemView);

            tagThumbnail = itemView.findViewById(R.id.gallery_thumbnail);
            tagTitle = itemView.findViewById(R.id.tag_title);
            photoCounter = itemView.findViewById(R.id.photo_counter);
        }
    }

    public void setData(List<hsgildt.nl.gildtapp.model.Tag> data)
    {
        this.data = data;
        notifyDataSetChanged();
    }
}
