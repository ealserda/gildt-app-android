package hsgildt.nl.gildtapp.view;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Toast;

import java.util.List;
import hsgildt.nl.gildtapp.Navigation;
import hsgildt.nl.gildtapp.R;
import hsgildt.nl.gildtapp.RetrofitBuilder;
import hsgildt.nl.gildtapp.model.Tag;
import hsgildt.nl.gildtapp.services.GalleryService;
import hsgildt.nl.gildtapp.view.adapter.GalleryRecyclerViewAdapter;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class GalleryActivity extends Navigation implements Callback<List<Tag>>, SwipeRefreshLayout.OnRefreshListener
{
    private RecyclerView recyclerView;
    private GalleryRecyclerViewAdapter adapter;
    private GalleryService galleryService;
    private SwipeRefreshLayout swipeRefreshLayout;

    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_gallery);
        super.setupNavigation();
        setupLayout();

        this.galleryService = RetrofitBuilder.getInstance().getRetrofitBuilder().create(GalleryService.class);

        loadGallery();
    }

    public void setupLayout()
    {
        this.navigationView.setCheckedItem(R.id.nav_gallary);
        this.setTitle(R.string.pictures);
        recyclerView = (RecyclerView) findViewById(R.id.gallery_recyclerview);
        GridLayoutManager gridLayoutManager = new GridLayoutManager(this, 2);
        recyclerView.setLayoutManager(gridLayoutManager);
        adapter = new GalleryRecyclerViewAdapter(this);
        recyclerView.setAdapter(adapter);
        this.swipeRefreshLayout = findViewById(R.id.gallery_swipe_refresh_layout);
        this.swipeRefreshLayout.setColorSchemeResources(R.color.colorPrimary,
                android.R.color.holo_green_dark,
                android.R.color.holo_orange_dark,
                android.R.color.holo_blue_dark);
        this.swipeRefreshLayout.setOnRefreshListener(this);
    }

    public void loadGallery()
    {
        swipeRefreshLayout.setRefreshing(true);
        galleryService.tags().enqueue(this);
    }

    @Override
    public void onResponse(Call<List<Tag>> call, Response<List<Tag>> response)
    {
        if (response.isSuccessful() && response.body() != null)
        {
            swipeRefreshLayout.setRefreshing(false);
            adapter.setData(response.body());
        }
        else
        {
            switch(response.code())
            {
                case 404:
                    Toast.makeText(GalleryActivity.this, R.string.cant_find_server, Toast.LENGTH_LONG).show();
                    break;
                case 500:
                    Toast.makeText(GalleryActivity.this, R.string.internal_error, Toast.LENGTH_LONG).show();
                    break;
                default:
                    Toast.makeText(GalleryActivity.this, R.string.general_error, Toast.LENGTH_LONG).show();
            }
        }
    }

    @Override
    public void onFailure(Call<List<Tag>> call, Throwable t)
    {
        swipeRefreshLayout.setRefreshing(false);
        Snackbar.make(swipeRefreshLayout, R.string.failure, Snackbar.LENGTH_INDEFINITE).setAction(
                R.string.retry, new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        loadGallery();
                    }
                }).show();
    }

    public void gotoDetailView(int id, String name)
    {
        Intent galleryDetailActivity2 = new Intent(this, GalleryDetailActivity.class);
        galleryDetailActivity2.putExtra("Tag ID", Integer.toString(id));
        galleryDetailActivity2.putExtra("Tag name", name);
        startActivity(galleryDetailActivity2);
    }

    @Override
    public void onRefresh()
    {
        loadGallery();
    }
}
