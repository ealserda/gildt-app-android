package hsgildt.nl.gildtapp.view;

import android.Manifest;
import android.annotation.TargetApi;
import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Build;
import android.provider.MediaStore;
import android.support.v4.app.ActivityCompat;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Toast;
import android.support.v4.content.CursorLoader;

import java.io.File;
import java.io.IOException;
import java.net.URI;

import hsgildt.nl.gildtapp.R;
import hsgildt.nl.gildtapp.RetrofitBuilder;
import hsgildt.nl.gildtapp.model.UploadResponse;
import hsgildt.nl.gildtapp.services.GalleryService;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class UploadActivity extends AppCompatActivity implements Callback<UploadResponse>
{
    private Button fileChooser;
    private EditText description;
    private EditText tags;
    private Button uploadFoto;
    private Uri uri;
    private GalleryService galleryService;
    private String tagID;
    private String tagName;
    private ImageView previewImage;
    private RelativeLayout uploadingScreen;

    public static final int GET_FROM_GALLERY = 13;

    protected boolean shouldAskPermissions()
    {
        return (Build.VERSION.SDK_INT > Build.VERSION_CODES.LOLLIPOP_MR1);
    }

    @TargetApi(23)
    protected void askPermissions()
    {
        String[] permissions = {
                "android.permission.READ_EXTERNAL_STORAGE",
                "android.permission.WRITE_EXTERNAL_STORAGE" };
        int requestCode = 200;
        requestPermissions(permissions, requestCode);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_upload);
        setupLayout();

        if(getIntent().hasExtra("Tag ID") && getIntent().hasExtra("Tag name"))
        {
            Intent intent = getIntent();
            tagID = intent.getStringExtra("Tag ID");
            tagName = intent.getStringExtra("Tag name");
            uploadFoto.setText(getApplicationContext().getString(R.string.upload_photo_to) + tagName);
        }

        if (shouldAskPermissions())
        {
            askPermissions();
        }

        this.galleryService = RetrofitBuilder.getInstance().getRetrofitBuilder().create(GalleryService.class);
    }

    public void setupLayout()
    {
        setTitle(R.string.upload);
        uploadFoto = (Button) findViewById(R.id.photo_upload_upload);
        fileChooser = (Button) findViewById(R.id.photo_upload_chooser);
        previewImage = (ImageView) findViewById(R.id.image_preview);
        uploadingScreen = (RelativeLayout) findViewById(R.id.uploadingScreen);
        description = (EditText) findViewById(R.id.photo_upload_description);
        fileChooser.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                Intent i = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                i.putExtra(Intent.EXTRA_LOCAL_ONLY, true);
                startActivityForResult(i, 100);
            }
        });
        uploadFoto.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                if (uri != null)
                {
                    uploadFile(uri);
                }
                else
                {
                    Toast.makeText(UploadActivity.this, R.string.choose_pic_first, Toast.LENGTH_LONG).show();
                }
            }
        });
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data)
    {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 100 && resultCode == RESULT_OK && data != null)
        {
            uri = data.getData();
            try
            {
                Bitmap bitmap = MediaStore.Images.Media.getBitmap(this.getContentResolver(), uri);
                previewImage.setImageBitmap(bitmap);
            } catch (IOException e)
            {
                e.printStackTrace();
            }
        }
    }

    public void uploadFile(Uri fileUri)
    {
        String descriptionText = description.getText().toString();
        int tagInt = Integer.parseInt(tagID);
        File file = new File(getRealPathFromURI(fileUri));

        if(descriptionText.isEmpty() || !isSafe(descriptionText))
        {
            descriptionText = getApplicationContext().getString(R.string.no_description);
        }

        RequestBody fileReqBody = RequestBody.create(MediaType.parse("image/*"), file);
        MultipartBody.Part image = MultipartBody.Part.createFormData("image", file.getName(), fileReqBody);
        RequestBody description = RequestBody.create(MediaType.parse("text/plain"), descriptionText);
        RequestBody tag = RequestBody.create(MediaType.parse("text/plain"), String.valueOf(tagInt));

        uploadingScreen.setVisibility(View.VISIBLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE,
                WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);

        galleryService.uploadImage(image, description, tag).enqueue(this);
    }

    public boolean isSafe(String text)
    {
        if(text.length() > 140)
        {
            return false;
        }
        else
        {
            return true;
        }
    }

    private String getRealPathFromURI(Uri contentUri)
    {
        String[] proj = {MediaStore.Images.Media.DATA};
        CursorLoader loader = new CursorLoader(this, contentUri, proj, null, null, null);
        Cursor cursor = loader.loadInBackground();
        int column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
        cursor.moveToFirst();
        String result = cursor.getString(column_index);
        cursor.close();
        return result;
    }

    @Override
    public void onResponse(Call<UploadResponse> call, Response<UploadResponse> response)
    {
        if (response.isSuccessful())
        {
            redirectAfterUpload(true);
        }
        else
        {
            switch(response.code())
            {
                case 404:
                    Toast.makeText(UploadActivity.this, R.string.cant_find_server, Toast.LENGTH_LONG).show();
                    redirectAfterUpload(false);
                    break;
                case 500:
                    Toast.makeText(UploadActivity.this, R.string.internal_error, Toast.LENGTH_LONG).show();
                    redirectAfterUpload(false);
                    break;
                default:
                    Toast.makeText(UploadActivity.this, R.string.general_error, Toast.LENGTH_LONG).show();
                    redirectAfterUpload(false);
            }
        }
    }

    @Override
    public void onFailure(Call<UploadResponse> call, Throwable t)
    {
        redirectAfterUpload(false);
    }

    public void redirectAfterUpload(boolean uploaded)
    {
        uploadingScreen.setVisibility(View.GONE);
        getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
        Intent galleryDetailActivity = new Intent(this, GalleryDetailActivity.class);
        galleryDetailActivity.putExtra("Tag ID", tagID);
        galleryDetailActivity.putExtra("Tag name", tagName);
        if(uploaded)
        {
            galleryDetailActivity.putExtra("Uploaded", "true");
        }
        else
        {
            galleryDetailActivity.putExtra("Uploaded", "false");
        }
        startActivity(galleryDetailActivity);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item)
    {
        switch (item.getItemId())
        {
            case android.R.id.home:
                finish();
                break;
        }
        return true;
    }
}
