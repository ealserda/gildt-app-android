package hsgildt.nl.gildtapp.view.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v4.view.PagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ScrollView;
import android.widget.TextView;


import hsgildt.nl.gildtapp.GildtApplication;
import hsgildt.nl.gildtapp.R;

public class OnboardingAdapter extends PagerAdapter {

    Context context;
    LayoutInflater layoutInflater;

    public OnboardingAdapter(Context context) {
        this.context = context;
    }

    public int[] onboarding_images = {
            R.mipmap.gildt_logo_groen,
            R.mipmap.screenshot_agenda,
            R.mipmap.screenshot_deals,
            R.mipmap.screenshot_stempelkaart,
            R.mipmap.screenshot_jukebox,
            R.mipmap.screenshot_gallery
    };

    public String[] onboarding_titles = {
            GildtApplication.getAppContext().getString(R.string.onboarding_welcome),
            GildtApplication.getAppContext().getString(R.string.onboarding_agenda),
            GildtApplication.getAppContext().getString(R.string.onboarding_deals),
            GildtApplication.getAppContext().getString(R.string.onboarding_stempelkaart),
            GildtApplication.getAppContext().getString(R.string.onboarding_jukebox),
            GildtApplication.getAppContext().getString(R.string.onboarding_photos)
    };

    public String[] onboarding_description = {
            GildtApplication.getAppContext().getString(R.string.onboarding_welcome_description),
            GildtApplication.getAppContext().getString(R.string.onboarding_agenda_description),
            GildtApplication.getAppContext().getString(R.string.onboarding_deals_description),
            GildtApplication.getAppContext().getString(R.string.onboarding_stempelkaart_description),
            GildtApplication.getAppContext().getString(R.string.onboarding_jukebox_description),
            GildtApplication.getAppContext().getString(R.string.onboarding_photos_description),

    };

    @Override
    public  Object instantiateItem(ViewGroup container, int position) {
        layoutInflater = (LayoutInflater) context.getSystemService(context.LAYOUT_INFLATER_SERVICE);
        View view = layoutInflater.inflate(R.layout.slide_layout, container, false);

        ImageView onboardingImageView = view.findViewById(R.id.onboarding_slide_image);
        TextView onboardingTitleTextView = view.findViewById(R.id.onboarding_slide_title);
        TextView onboardingDescriptionTextView = view.findViewById(R.id.onboarding_slide_description);

        onboardingImageView.setImageResource(onboarding_images[position]);
        onboardingTitleTextView.setText(onboarding_titles[position]);
        onboardingDescriptionTextView.setText(onboarding_description[position]);

        container.addView(view);

        return view;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((ScrollView)object);
    }

    @Override
    public int getCount() {
        return onboarding_images.length;
    }

    @Override
    public boolean isViewFromObject(@NonNull View view, @NonNull Object o) {
        return view == (ScrollView) o;
    }
}
