package hsgildt.nl.gildtapp.view;

import android.content.Intent;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.Toast;

import hsgildt.nl.gildtapp.R;
import hsgildt.nl.gildtapp.RetrofitBuilder;
import hsgildt.nl.gildtapp.model.Jukebox;
import hsgildt.nl.gildtapp.model.Song;
import hsgildt.nl.gildtapp.services.JukeboxService;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class JukeboxAddActivity extends AppCompatActivity implements Callback<Jukebox>
{
    private Button setSong;
    private EditText title;
    private EditText artist;
    private JukeboxService jukeboxService;
    private RelativeLayout uploadingScreen;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_jukebox_add);
        setupLayout();

        this.jukeboxService = RetrofitBuilder.getInstance().getRetrofitBuilder().create(JukeboxService.class);
    }

    public void setupLayout()
    {
        setTitle(R.string.add_song);
        setSong = findViewById(R.id.setSong);
        setSong.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                setRequestedSong();
            }
        });
        uploadingScreen = (RelativeLayout) findViewById(R.id.uploadingScreen);
        title = findViewById(R.id.setTitle);
        artist = findViewById(R.id.setArtist);
    }

    public void setRequestedSong()
    {
        Song addSong = new Song(title.getText().toString(), artist.getText().toString());

        uploadingScreen.setVisibility(View.VISIBLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE,
                WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
        jukeboxService.addSong(addSong).enqueue(this);
    }

    @Override
    public boolean onSupportNavigateUp()
    {
        onBackPressed();
        return true;
    }

    @Override
    public void onResponse(Call<Jukebox> call, Response<Jukebox> response)
    {
        if (response.isSuccessful() && response.body() != null)
        {
            uploadingScreen.setVisibility(View.GONE);
            getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
            Intent jukeboxActivity = new Intent(this, JukeboxActivity.class);
            jukeboxActivity.putExtra("Added song", response.body());
            startActivity(jukeboxActivity);
        }
        else
        {
            switch(response.code())
            {
                case 404:
                    Toast.makeText(JukeboxAddActivity.this, R.string.cant_find_server, Toast.LENGTH_LONG).show();
                    break;
                case 500:
                    Toast.makeText(JukeboxAddActivity.this, R.string.internal_error, Toast.LENGTH_LONG).show();
                    break;
                default:
                    Toast.makeText(JukeboxAddActivity.this, R.string.general_error, Toast.LENGTH_LONG).show();
            }
        }

    }

    @Override
    public void onFailure(Call<Jukebox> call, Throwable t)
    {
        Toast.makeText(this, R.string.voting_error, Toast.LENGTH_LONG).show();
        uploadingScreen.setVisibility(View.GONE);
        getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
    }
}
