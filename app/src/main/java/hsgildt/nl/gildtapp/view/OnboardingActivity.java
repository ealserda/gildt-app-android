package hsgildt.nl.gildtapp.view;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.text.Html;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.ViewFlipper;

import hsgildt.nl.gildtapp.GildtApplication;
import hsgildt.nl.gildtapp.Navigation;
import hsgildt.nl.gildtapp.R;
import hsgildt.nl.gildtapp.view.adapter.OnboardingAdapter;

public class OnboardingActivity extends Activity {
    private ViewPager onboardingViewPager;
    private LinearLayout dotsLinearLayout;
    private OnboardingAdapter onboardingAdapter;
    private TextView[] dots;
    private Button skipButton;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_onboarding);

        setupLayout();
      }

      private void setupLayout() {
          onboardingViewPager = findViewById(R.id.onboarding_view_pager);
          dotsLinearLayout = findViewById(R.id.onboarding_dots_linear_layout);
          skipButton = findViewById(R.id.skip_button);

          onboardingAdapter = new OnboardingAdapter(this);
          onboardingViewPager.setAdapter(onboardingAdapter);

          addDotsIndicator(0);

          onboardingViewPager.addOnPageChangeListener(viewListener);
      }

      public void skipButton(View v) {
          Intent i = new Intent(this, RegisterActivity.class);
          startActivity(i);

          SharedPreferences.Editor editor = GildtApplication.getSharedPreferences().edit();
          editor.putBoolean("onboardingSkip", true);
          editor.commit();
      }

      public void addDotsIndicator(int position) {
        dots = new TextView[6];
        dotsLinearLayout.removeAllViews();

        for(int i = 0; i < dots.length; i++) {
            dots[i] = new TextView(this);
            dots[i].setText(Html.fromHtml("&#8226;"));
            dots[i].setTextSize(20);
            dots[i].setPadding(0, 0, 20, 0);
            dots[i].setTextColor(Color.WHITE);

            dotsLinearLayout.addView(dots[i]);
        }

        if(dots.length > 0) {
            dots[position].setTextSize(35);
        }
      }

      ViewPager.OnPageChangeListener viewListener = new ViewPager.OnPageChangeListener() {

          @Override
          public void onPageSelected(int i) {
            addDotsIndicator(i);
            if(i == dots.length - 1) {
                skipButton.setText(R.string.onboarding_begin);
            } else {
                skipButton.setText(R.string.onboarding_skip);
            }
          }

          @Override
          public void onPageScrolled(int i, float v, int i1) { }

          @Override
          public void onPageScrollStateChanged(int i) { }
      };

}
