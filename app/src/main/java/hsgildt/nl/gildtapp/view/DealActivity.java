package hsgildt.nl.gildtapp.view;

import android.content.Context;
import android.graphics.Point;
import android.os.Build;
import android.os.Bundle;
import android.os.VibrationEffect;
import android.os.Vibrator;
import android.support.design.widget.Snackbar;
import android.view.Display;
import android.view.MotionEvent;
import android.view.View;

import com.yarolegovich.discretescrollview.DiscreteScrollView;

import java.util.ArrayList;
import java.util.List;

import hsgildt.nl.gildtapp.Navigation;
import hsgildt.nl.gildtapp.R;
import hsgildt.nl.gildtapp.RetrofitBuilder;
import hsgildt.nl.gildtapp.model.Deal;
import hsgildt.nl.gildtapp.services.DealService;
import hsgildt.nl.gildtapp.view.adapter.DealRecyclerViewAdapter;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class DealActivity extends Navigation implements Callback<List<Deal>>, View.OnTouchListener {
	float startPositonY, dY;
	private DealService dealService;
	private List<Deal> deals;
	int cardWidth;
	DiscreteScrollView scrollView;
	private boolean isRedeeming;

	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_deal);
		super.setupNavigation();

		setTitle(R.string.deal_title);

        deals = new ArrayList<>();
        dealService = RetrofitBuilder.getInstance().getRetrofitBuilder().create(DealService.class);

		setupLayout();
		loadDeals();
	}

	private void setupLayout() {
		navigationView.setCheckedItem(R.id.nav_deals);

		Display display = getWindowManager(). getDefaultDisplay();
		Point size = new Point();
		display.getSize(size);
		cardWidth = size.x - dpToPx(70);

		scrollView = findViewById(R.id.deal_picker);
		scrollView.setAdapter(new DealRecyclerViewAdapter(this, this.deals, cardWidth));
		scrollView.setSlideOnFling(true);
	}

	private void loadDeals(){
        dealService.deals().enqueue(this);
    }

	private void vibrate (int ms) {
		Vibrator v = (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);
		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
			v.vibrate(VibrationEffect.createOneShot(ms, VibrationEffect.DEFAULT_AMPLITUDE));
		} else {
			v.vibrate(ms);
		}
	}

	// Redeem deal and handling responses
	public void redeem_deal(final Deal deal, final DealRecyclerViewAdapter.ViewHolder viewHolder) {
		dealService.redeemDeal(deal.id).enqueue(new Callback<Deal>() {
			@Override
			public void onResponse(Call<Deal> call, Response<Deal> response) {
				if(response.isSuccessful()) {
					deal.deals_left = response.body().deals_left;
					vibrate(200);

					if (deal.deals_left != null) {
						viewHolder.dealsLeft.setText(getString(R.string.deal_usage_left, deal.deals_left));
					}

					viewHolder.changeAndShowState(getString(R.string.deal_redeemd),
							DealRecyclerViewAdapter.DealState.SUCCESS, 2000 );
				} else {
					vibrate(600);
					viewHolder.changeAndShowState(getString(R.string.deal_not_available),
							DealRecyclerViewAdapter.DealState.ERROR, 4000 );
				}
			}

			@Override
			public void onFailure(Call<Deal> call, Throwable t) {
				vibrate(600);
				viewHolder.changeAndShowState(getString(R.string.failure),
						DealRecyclerViewAdapter.DealState.NETWORK_FAILURE, 4000 );
			}
		});
	}

	// Handling deal swipe up, y position <= -20? Redeem deal...
	private void movedUpHandler(View view, MotionEvent event, Deal deal, DealRecyclerViewAdapter.ViewHolder viewHolder) {
		if((event.getRawY() + dY) <= -20) {
			if(!isRedeeming) {
				view.getParent().requestDisallowInterceptTouchEvent(true);
				isRedeeming = true;

				viewHolder.dealStateOverlay.setTextColor(getResources().getColor(R.color.colorPrimary));
				viewHolder.dealStateOverlay.setVisibility(View.VISIBLE);
				viewHolder.dealStateOverlay.setText(R.string.deal_is_redeeming);
				redeem_deal(deal, viewHolder);
				view.animate().y(startPositonY + dpToPx(50)).setDuration(100).start();
			} else {
				view.getParent().requestDisallowInterceptTouchEvent(false);
			}
		} else if (event.getRawY() + dY <= startPositonY + dpToPx(50)){
			if(event.getRawY() + dY <= startPositonY + dpToPx(40)){
				view.getParent().requestDisallowInterceptTouchEvent(true);
			}
			view.animate()
					.y(event.getRawY() + dY)
					.setDuration(0)
					.start();
		}
	}

	@Override
	public boolean onTouch(View view, MotionEvent event) {
		int currentItemPosition = scrollView.getCurrentItem();
		DealRecyclerViewAdapter.ViewHolder viewHolder = (DealRecyclerViewAdapter.ViewHolder) scrollView.getViewHolder(currentItemPosition);
		if (viewHolder.itemView == view) {
			Deal deal = deals.get(currentItemPosition);
			if((deal.deal_type.equals("timely")) || (deal.deal_type.equals("usage") && deal.deals_left > 0)) {
				switch (event.getAction()) {
					case MotionEvent.ACTION_DOWN:
						dY = view.getY() - event.getRawY();
						isRedeeming = false;
						break;

					case MotionEvent.ACTION_MOVE:
						movedUpHandler(view, event, deal, viewHolder);
						break;
					case MotionEvent.ACTION_CANCEL:
						view.animate().y(startPositonY + dpToPx(50)).setDuration(500).start();
						break;
					case MotionEvent.ACTION_UP:
						view.animate().y(startPositonY + dpToPx(50)).setDuration(500).start();
						break;
					default:
						return true;
				}
			}
		}
		return true;
	}

	@Override
	protected void onResume() {
		super.onResume();
		this.navigationView.setCheckedItem(R.id.nav_deals);
	}

	@Override
	public void onResponse(Call<List<Deal>> call, Response<List<Deal>> response) {
		if (response.isSuccessful() && response.body() != null) {
			deals.clear();
			deals.addAll(response.body());
			scrollView.getAdapter().notifyDataSetChanged();
		}
	}

	@Override
	public void onFailure(Call<List<Deal>> call, Throwable t) {
		Snackbar.make(scrollView, R.string.failure, Snackbar.LENGTH_INDEFINITE).setAction(
				R.string.retry, new View.OnClickListener() {
					@Override
					public void onClick(View v) {
						loadDeals();
					}
				}).show();
	}
}
