package hsgildt.nl.gildtapp.view;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.SparseArray;
import android.view.Menu;
import android.view.MenuItem;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;
import android.widget.Toast;

import com.google.android.gms.vision.CameraSource;
import com.google.android.gms.vision.Detector;
import com.google.android.gms.vision.barcode.Barcode;
import com.google.android.gms.vision.barcode.BarcodeDetector;

import java.io.IOException;

import hsgildt.nl.gildtapp.R;


public class QrScannerActivity extends AppCompatActivity implements QrDialog.QrDialogListener, SurfaceHolder.Callback, Detector.Processor<Barcode> {

    private SurfaceView surfaceView;
    private CameraSource cameraSource;
    private BarcodeDetector barcodeDetector;
    private long qrErrorMessageDisplayedAt = System.currentTimeMillis() - 2000;
    final int MY_PERMISSIONS_REQUEST_CAMERA=0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_qr_scanner);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        this.surfaceView = findViewById(R.id.qr_scanner_preview);

        barcodeDetector = new BarcodeDetector.Builder(this)
                .setBarcodeFormats(Barcode.QR_CODE).build();

        cameraSource = new CameraSource.Builder(this, barcodeDetector)
                .setRequestedPreviewSize(640, 480).build();

        surfaceView.getHolder().addCallback(this);
        barcodeDetector.setProcessor(this);

        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

    }

    // Try to convert qr code into integer
    public void convertQrCode (String qrCode) {
        try {
            int qrCodeI = Integer.parseInt(qrCode);
            Intent resultIntent = new Intent();
            resultIntent.putExtra("qr_code", qrCodeI);
            setResult(StampsActivity.RESULT_OK, resultIntent);
            finish();
        } catch (Exception e){
            InvalidQrcode();
        }
    }

    public void InvalidQrcode () {
        // avoid spamming error message
        if(this.qrErrorMessageDisplayedAt + 2000 < System.currentTimeMillis()) {
            this.qrErrorMessageDisplayedAt = System.currentTimeMillis();
            runOnUiThread(new Runnable() {
                public void run() {
                    Toast.makeText(QrScannerActivity.this, R.string.qr_scanner_invalid, Toast.LENGTH_SHORT).show();
                }
            });
        }
    }

    @Override
    public void surfaceCreated(SurfaceHolder holder) {
        if (ActivityCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(QrScannerActivity.this, new String[] {Manifest.permission.CAMERA}, MY_PERMISSIONS_REQUEST_CAMERA);
            return;
        } else {
            try {
                cameraSource.start(holder);
            } catch (IOException e){
            }
        }
    }

    @Override
    public void surfaceChanged(SurfaceHolder holder, int format, int width, int height) {

    }

    @Override
    public void surfaceDestroyed(SurfaceHolder holder) {
        cameraSource.stop();
    }

    @Override
    public void receiveDetections(Detector.Detections<Barcode> detections) {
        SparseArray<Barcode> qrCodes = detections.getDetectedItems();

        if(qrCodes.size() != 0) {
            convertQrCode(qrCodes.valueAt(0).displayValue);
        }
    }

    @Override
    public void release() {

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.qr_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.qr_menu_manual) {
            QrDialog qrDialog = new QrDialog();
            qrDialog.show(getSupportFragmentManager(), "qr dialog");
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void applyQrCode(String qrCode) {
        convertQrCode(qrCode);
    }

}
