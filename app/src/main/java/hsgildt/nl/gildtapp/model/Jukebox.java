package hsgildt.nl.gildtapp.model;

import android.os.Parcel;
import android.os.Parcelable;

public class Jukebox implements Parcelable {

    public int id;
    public String title;
    public String artist;
    public int user_id;
    public int votes;
    public int did_vote;

    public Jukebox(int id, String title, String artist, int user_id, int votes, int did_vote)
    {
        this.id = id;
        this.title = title;
        this.artist = artist;
        this.user_id = user_id;
        this.votes = votes;
        this.did_vote = did_vote;
    }

    @Override
    public int describeContents()
    {
        return hashCode();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags)
    {
        dest.writeInt(id);
        dest.writeString(title);
        dest.writeString(artist);
        dest.writeInt(user_id);
        dest.writeInt(votes);
        dest.writeInt(did_vote);
    }

    public Jukebox(Parcel parcel)
    {
        id = parcel.readInt();
        title = parcel.readString();
        artist = parcel.readString();
        user_id = parcel.readInt();
        votes = parcel.readInt();
        did_vote = parcel.readInt();


    }

    public static final Parcelable.Creator<Jukebox> CREATOR = new Parcelable.Creator<Jukebox>()
    {
        @Override
        public Jukebox createFromParcel(Parcel parcel)
        {
            return new Jukebox(parcel);
        }

        @Override
        public Jukebox[] newArray(int size) {
            return new Jukebox[0];
        }
    };
}
