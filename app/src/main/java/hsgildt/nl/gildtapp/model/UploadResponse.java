package hsgildt.nl.gildtapp.model;

import java.util.ArrayList;

public class UploadResponse {

    private String description;
    private Photo image;
    private String publish_date;
    private String publisher;
    private ArrayList<TagSmall> tags;

    public UploadResponse(String description, Photo image, String publish_date, String publisher, ArrayList<TagSmall> tags)
    {
        this.description = description;
        this.image = image;
        this.publish_date = publish_date;
        this.publisher = publisher;
        this.tags = tags;
    }
}
