package hsgildt.nl.gildtapp.model;

import android.os.Parcel;
import android.os.Parcelable;

public class Tag {

    public int id;
    public String title;
    public Photo preview_image;
    public int number_of_images;


    public Tag(int id, String title, Photo preview_image, int number_of_images)
    {
        this.id = id;
        this.title = title;
        this.preview_image = preview_image;
        this.number_of_images = number_of_images;
    }

}
