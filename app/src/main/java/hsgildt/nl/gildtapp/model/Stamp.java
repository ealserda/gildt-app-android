package hsgildt.nl.gildtapp.model;

public class Stamp {
    public int id;
    public String event_date;
    public String event_date_humanized;
    public Boolean verified_attendance;
}
