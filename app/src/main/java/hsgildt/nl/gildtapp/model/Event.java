package hsgildt.nl.gildtapp.model;

import android.annotation.SuppressLint;
import android.os.Parcel;
import android.os.Parcelable;

public class Event implements Parcelable {

    public int id;
    public String image;
    public String title;
    public String description;
    public String short_description;
    public String event_date;
    public Boolean theme_party;
    public Boolean attendance;
    public String[] attendance_users;

    protected Event(Parcel in) {
        id = in.readInt();
        image = in.readString();
        title = in.readString();
        description = in.readString();
        short_description = in.readString();
        event_date = in.readString();
        byte tmpTheme_party = in.readByte();
        theme_party = tmpTheme_party == 0 ? null : tmpTheme_party == 1;
        byte tmpAttendance = in.readByte();
        attendance = tmpAttendance == 0 ? null : tmpAttendance == 1;
        attendance_users = in.createStringArray();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(id);
        dest.writeString(image);
        dest.writeString(title);
        dest.writeString(description);
        dest.writeString(short_description);
        dest.writeString(event_date);
        dest.writeByte((byte) (theme_party == null ? 0 : theme_party ? 1 : 2));
        dest.writeByte((byte) (attendance == null ? 0 : attendance ? 1 : 2));
        dest.writeStringArray(attendance_users);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<Event> CREATOR = new Creator<Event>() {
        @Override
        public Event createFromParcel(Parcel in) {
            return new Event(in);
        }

        @Override
        public Event[] newArray(int size) {
            return new Event[size];
        }
    };

    public static class Attendance {
        public Boolean attendance;
        public  Attendance(Boolean attendance) {
            this.attendance = attendance;
        }
    }
}
