package hsgildt.nl.gildtapp.model;

public class Song
{

    public String title;
    public String artist;

    public Song(String title, String artist)
    {
        this.title = title;
        this.artist = artist;
    }
}

