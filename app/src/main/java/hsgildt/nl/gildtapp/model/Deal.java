package hsgildt.nl.gildtapp.model;

public class Deal {

    public int id;
    public String image;
    public String title;
    public String description;
    public String deal_type;
    public String start_date;
    public String expire_date;
    public String expire_date_humanized;
    public Integer available_amount;
    public Integer deals_left;
}
