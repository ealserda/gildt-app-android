package hsgildt.nl.gildtapp.model;

public class User {

    public String email;
    public String username;
    public String password;
    public String password_confirmation;

    public User (String email, String password) {
        this.email = email;
        this.password = password;
    }

    public User (String email, String username, String password, String password_confirmation) {
        this.email = email;
        this.username = username;
        this.password = password;
        this.password_confirmation = password_confirmation;
    }

    public class Response {
        public String jwt;
        public String username;
        public String email;
        public String notification;
        public String message;
    }

    public static class Fcm {
        public String fcm_token;

        public Fcm (String fcm_token) {
            this.fcm_token = fcm_token;
        }
    }
}
