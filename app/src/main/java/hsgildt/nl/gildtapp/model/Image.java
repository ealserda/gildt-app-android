package hsgildt.nl.gildtapp.model;

import android.os.Parcel;
import android.os.Parcelable;

public class Image {

    public String description;
    public Photo image;
    public String publish_date;
    public String publisher;

    public Image(String description, Photo image, String publish_date, String publisher) {
        this.description = description;
        this.image = image;
        this.publish_date = publish_date;
        this.publisher = publisher;
    }

}