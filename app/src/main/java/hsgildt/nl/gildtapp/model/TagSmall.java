package hsgildt.nl.gildtapp.model;

public class TagSmall {

    private int id;
    private String title;

    public TagSmall(int id, String title)
    {
        this.id = id;
        this.title = title;
    }
}
