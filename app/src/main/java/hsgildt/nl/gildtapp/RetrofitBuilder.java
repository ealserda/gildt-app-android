package hsgildt.nl.gildtapp;

import android.content.Intent;
import android.util.Log;

import com.jakewharton.espresso.OkHttp3IdlingResource;

import java.io.IOException;

import hsgildt.nl.gildtapp.view.LoginActivity;
import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

import static android.support.v4.content.ContextCompat.startActivity;

public class RetrofitBuilder {
    private static RetrofitBuilder instance;
    private Retrofit retrofitBuilder;
    public OkHttp3IdlingResource idlingResource;
    private static final String BASE_URL = "https://gildt.inholland-informatica.nl/api/v1/";

    protected RetrofitBuilder() {
        OkHttpClient okClient = new OkHttpClient.Builder()
            .addInterceptor(new ServiceInterceptor())
            .build();

        // Let Espresso know when async task is done...
        idlingResource = OkHttp3IdlingResource.create("OkHttp", okClient);

        retrofitBuilder = new Retrofit.Builder()
                .baseUrl(BASE_URL)
                .client(okClient)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
    }

    // Using singleton for the retrofit requests
    // Having the same OkHttpClient gives some http caching benefits
    public static RetrofitBuilder getInstance() {
        if(instance == null) {
            instance = new RetrofitBuilder();
        }
        return instance;
    }

    public Retrofit getRetrofitBuilder (){
        return this.retrofitBuilder;
    }

    // Retrofit interceptor, add auth token when needed and handle 401 gobal
    class ServiceInterceptor implements Interceptor {

        @Override
        public Response intercept(Interceptor.Chain chain) throws IOException {
            Request request = chain.request();

            String authToken = AuthController.getInstance().getAuthToken();
            if (request.header("No-Authentication") == null && authToken != null) {
                request = request.newBuilder()
                        .addHeader("Authorization", "Bearer " + authToken)
                        .build();
            }

            Response response =  chain.proceed(request);
            if (response.code() == 401){
                // 401? Token expired? -> logout and redirect to login page
                AuthController.getInstance().logout();
                Intent n = new Intent(GildtApplication.getAppContext(), LoginActivity.class);
                n.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                GildtApplication.getAppContext().startActivity(n);
            }
            return response;
        }
    }
}
