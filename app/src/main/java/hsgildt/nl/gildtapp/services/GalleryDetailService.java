package hsgildt.nl.gildtapp.services;

import java.util.List;

import hsgildt.nl.gildtapp.model.Image;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;

public interface GalleryDetailService
{
    @GET("tag/{tagID}/images")
    Call<List<Image>> imagesForTag(@Path("tagID") int tagID);
}
