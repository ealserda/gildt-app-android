package hsgildt.nl.gildtapp.services;

import java.util.List;

import hsgildt.nl.gildtapp.model.Stamp;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Path;

public interface StampService {

    @GET("stamp_card")
    Call<List<Stamp>> stamp_card();

    @POST("stamp_card/{qrCode}")
    Call<Stamp> verify_attendance(@Path("qrCode") int qrCode);
}
