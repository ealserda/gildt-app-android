package hsgildt.nl.gildtapp.services;

import hsgildt.nl.gildtapp.model.User;
import retrofit2.Call;
import retrofit2.Response;
import retrofit2.http.Body;
import retrofit2.http.Headers;
import retrofit2.http.POST;
import retrofit2.http.PUT;

public interface AuthService {

    @POST("user_token")
    @Headers("No-Authentication: true")
    Call<User.Response> login(@Body User user);


    @POST("user")
    @Headers("No-Authentication: true")
    Call<User.Response> register(@Body User user);

    @PUT("user/fcm_token")
    Call<Response<Void>> updateFcmToken(@Body User.Fcm fcm);
}
