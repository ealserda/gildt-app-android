package hsgildt.nl.gildtapp.services;

import java.util.List;


import hsgildt.nl.gildtapp.model.Event;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Path;

public interface EventService {

	@GET("event")
	Call<List<Event>> events();

	@POST("event/{id}/attendance")
	Call<Event> update_attendance(@Path("id") int id, @Body Event.Attendance attendance);
}
