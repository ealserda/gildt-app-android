package hsgildt.nl.gildtapp.services;

import java.util.List;

import hsgildt.nl.gildtapp.model.Deal;
import hsgildt.nl.gildtapp.model.Event;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.PUT;
import retrofit2.http.Path;

public interface DealService {

    @GET("deal")
    Call<List<Deal>> deals();

    @PUT("deal/{id}/redeem")
    Call<Deal> redeemDeal(@Path("id") int id);

}
