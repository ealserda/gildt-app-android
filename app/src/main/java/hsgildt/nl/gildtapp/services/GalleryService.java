package hsgildt.nl.gildtapp.services;

import java.util.List;


import hsgildt.nl.gildtapp.model.Tag;
import hsgildt.nl.gildtapp.model.UploadResponse;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;

public interface GalleryService
{
    @GET("tag")
    Call<List<Tag>> tags();

    @Multipart
    @POST("image")
    Call<UploadResponse> uploadImage(@Part MultipartBody.Part photo, @Part("description") RequestBody description, @Part("tags") RequestBody tags);
}
