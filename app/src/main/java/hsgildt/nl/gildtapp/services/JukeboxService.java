package hsgildt.nl.gildtapp.services;

import java.util.List;

import hsgildt.nl.gildtapp.model.Jukebox;
import hsgildt.nl.gildtapp.model.Song;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Path;

public interface JukeboxService {

    @GET("song")
    Call<List<Jukebox>> songs();

    @POST("song")
    Call<Jukebox> addSong(@Body Song song);

    @PUT("song/{songID}/upvote")
    Call<Jukebox> voteSong(@Path("songID") int songID);

    @PUT("song/{songID}/downvote")
    Call<Jukebox> downvoteSong(@Path("songID") int songID);
}
