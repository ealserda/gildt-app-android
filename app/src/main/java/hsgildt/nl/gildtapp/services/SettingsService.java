package hsgildt.nl.gildtapp.services;

import hsgildt.nl.gildtapp.model.Notifications;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.PUT;

public interface SettingsService {

    @PUT("user/notification")
    Call<Notifications> changeNotificationSettings(@Body Notifications notification);
}
